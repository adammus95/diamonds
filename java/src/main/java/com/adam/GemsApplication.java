package com.adam;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class GemsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GemsApplication.class, args);

	}

}
