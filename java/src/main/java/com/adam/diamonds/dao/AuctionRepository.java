package com.adam.diamonds.dao;

import com.adam.diamonds.entity.Auction;
import com.adam.diamonds.entity.ItemStone;
import com.adam.diamonds.rest.requests.AuctionsRequestParams;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AuctionRepository extends JpaRepository<Auction, Long> {

    List<Auction> findAllByRemovedFalseAndExpirationTimeBefore(LocalDateTime localDateTime);
    List<Auction> findAllByItemStonesAndRemovedFalse(ItemStone itemStone);

    @Query("SELECT DISTINCT a FROM Auction a LEFT JOIN a.itemStones i LEFT JOIN a.bids b WHERE a.removed = false " +
            "and (:#{#params.stones} is null or i.stone in :#{#params.stones})" +
            "and (:#{#params.cuts} is null or i.cut in :#{#params.cuts})" +
            "and (:#{#params.minWeight} is null or :#{#params.minWeight} <= i.stone.weight) " +
            "and (:#{#params.maxWeight} is null or :#{#params.maxWeight} >= i.stone.weight)" +
            "and (:#{#params.minCondition} is null or :#{#params.minCondition} <= i.stone.condition)" +
            "and (:#{#params.maxCondition} is null or :#{#params.maxCondition} >= i.stone.condition)" +
            "and (:#{#params.minPrice} is null or (:#{#params.minPrice} <= a.price and (b = null or (b.highest = true and :#{#params.minPrice} <= b.price))))" +
            "and (:#{#params.maxPrice} is null or (:#{#params.maxPrice} >= a.price and (b = null or (b.highest = true and :#{#params.maxPrice} >= b.price))))")
    Page<Auction> findAll(@Param("params") AuctionsRequestParams auctionsRequestParams, Pageable pageable);

}
