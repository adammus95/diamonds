package com.adam.diamonds.dao;

import com.adam.diamonds.entity.Auction;
import com.adam.diamonds.entity.Bid;
import com.adam.diamonds.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BidRepository extends JpaRepository<Bid, Long> {

    Bid findByAuctionAndHighestTrue(Auction auction);
    Bid findByAuctionAndUserAndCurrentTrue(Auction auction, User user);

}
