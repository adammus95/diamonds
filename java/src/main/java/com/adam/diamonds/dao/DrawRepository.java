package com.adam.diamonds.dao;

import com.adam.diamonds.entity.Draw;
import com.adam.diamonds.entity.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface DrawRepository extends JpaRepository<Draw, Long> {

    Draw findTopByUserDetailOrderByDrawTimeDesc(UserDetail userDetail);
    List<Draw> findTop10ByDrawTimeBeforeOrderByDrawTimeDesc(LocalDateTime dateTime);

}
