package com.adam.diamonds.dao;

import com.adam.diamonds.entity.ItemStone;
import com.adam.diamonds.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemStoneRepository extends JpaRepository<ItemStone, Long>{

    Page<ItemStone> findAllByUserAndRemovedFalse(User user, Pageable pageable);
}
