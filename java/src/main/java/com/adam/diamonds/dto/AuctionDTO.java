package com.adam.diamonds.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AuctionDTO {
    private Long id;
    private BigDecimal price;
    private boolean biddable;
    private Integer views;
    private LocalDateTime expirationTime;
    private UserInfoDTO user;
    private List<ItemStoneDTO> itemStones;
    private List<BidDTO> bids;
}
