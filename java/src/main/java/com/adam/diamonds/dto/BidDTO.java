package com.adam.diamonds.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
public class BidDTO {

    private Long id;
    private boolean current;
    private LocalDateTime addTime;
    private BigDecimal price;
    private UserInfoDTO user;

}
