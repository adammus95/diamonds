package com.adam.diamonds.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class DrawDTO {

    private LocalDateTime drawTime;
    private ItemStoneDTO itemStone;

}