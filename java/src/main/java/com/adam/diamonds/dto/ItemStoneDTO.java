package com.adam.diamonds.dto;

import com.adam.diamonds.enums.Cut;
import com.adam.diamonds.enums.Stone;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ItemStoneDTO {

    private Long id;
    private Stone stone;
    private Cut cut;
    private Integer condition;
    private Double weight;
    private String color;
    private boolean available;
    private boolean removed;

}
