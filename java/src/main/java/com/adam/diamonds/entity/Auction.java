package com.adam.diamonds.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="auction")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class Auction {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private BigDecimal price;

    @Column
    private Integer views;

    @Column
    private boolean removed;

    @Column
    private boolean biddable;

    @Column
    private LocalDateTime addTime;

    @Column
    private LocalDateTime removeTime;

    @Column(name = "expiration_time")
    private LocalDateTime expirationTime;

    @ManyToMany
    @JoinTable(name = "auction_item",
            joinColumns = @JoinColumn(name = "auction_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private List<ItemStone> itemStones = new ArrayList<>();

    @OneToMany(mappedBy = "auction")
    private List<Bid> bids = new ArrayList<>();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="user_id")
    private User user;


}
