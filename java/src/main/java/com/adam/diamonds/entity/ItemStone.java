package com.adam.diamonds.entity;

import com.adam.diamonds.enums.Cut;
import com.adam.diamonds.enums.Stone;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="item_stone")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class ItemStone {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Double weight;

    @Column
    private Integer condition;

    @Column
    private String color;

    @Column
    @Enumerated(EnumType.STRING)
    private Cut cut;

    @Column
    @Enumerated(EnumType.STRING)
    private Stone stone;

    @Column
    private boolean removed;

    @Column
    private boolean available = true;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="user_id")
    private User user;

    @ManyToMany(mappedBy = "itemStones")
    private List<Auction> auctions = new ArrayList<>();

    @OneToOne(mappedBy = "itemStone", cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    private Draw draw;


}
