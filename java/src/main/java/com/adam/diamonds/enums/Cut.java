package com.adam.diamonds.enums;

import java.util.Random;

public enum Cut {
    NO_CUT_1, NO_CUT_2, NO_CUT_3, NO_CUT_4, NO_CUT_5, BRILLIANT, OVAL, OCTAGON;

    public static Cut randomNoCut() {
        Random rand = new Random();
        Integer result = rand.nextInt(5) + 1;
        Integer lucky = rand.nextInt(30) + 1;
        switch (lucky) {
            case 28: {
                return Cut.BRILLIANT;
            }
            case 29: {
                return Cut.OVAL;
            }
            case 30: {
                return Cut.OCTAGON;
            }
        }
        return Cut.valueOf("NO_CUT_" + result);
    }
}


