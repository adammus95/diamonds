package com.adam.diamonds.enums;

import java.util.Random;

public enum Stone {
    DIAMOND, RUBY, SAPPHIRE, EMERALD, AMETHYST;

    private static Integer PROPABILITY = 1000000000;

    public static Stone random(){
        return Stone.values()[new Random().nextInt(Stone.values().length)];
    }
    public static Double randomWeight() {

        Random rand = new Random();

        Integer size = rand.nextInt(PROPABILITY);
        Integer range = PROPABILITY / 2;
        Integer sum = range;

        while (size >= sum) {
            range /= 2;
            sum += range;
        }


        Integer propability = PROPABILITY / range;
//        Double weight = 0.01 * Math.pow(propability, 0.333);
        Double weight = 0.005 * propability;

        return weight;
    }
    public static String randomColor(Stone stone){
        Random rand = new Random();

        int minR=0;
        int maxR=255;
        int minG=0;
        int maxG=255;
        int minB=0;
        int maxB=255;

        int r=0;
        int g=0;
        int b=0;

        switch (stone){
            case RUBY: {
                minR = 180;
                r = rand.nextInt(maxR-minR) + minR;
                maxG = r-50;
                g = rand.nextInt(maxG-minG) + minG;
                b = g;
                break;
            }
            case DIAMOND: {
                minR = 240;
                r = rand.nextInt(maxR-minR) + minR;
                g = r;
                minB = 100;
                b = rand.nextInt(maxB-minB) + minB;
                break;
            }
            case EMERALD: {
                minG = 70;
                g = rand.nextInt(maxG-minG) + minG;
                maxR = g-50;
                r = rand.nextInt(maxR-minR) + minR;
                b = r;
                break;
            }
            case AMETHYST: {
                minR = 80;
                r = rand.nextInt(maxR-minR) + minR;
                b = r;
                maxG = b/2;
                g = rand.nextInt(maxG-minG) + minG;
                break;
            }
            case SAPPHIRE: {
                minB = 100;
                b = rand.nextInt(maxB-minB) + minB;
                maxR = b-90;
                r = rand.nextInt(maxR-minR) + minR;
                g = r;
                break;
            }
        }

        String R = String.format("%03d", r);
        String G = String.format("%03d", g);
        String B = String.format("%03d", b);

        return R+G+B;
    }
}
