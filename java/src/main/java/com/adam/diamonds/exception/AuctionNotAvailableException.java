package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class AuctionNotAvailableException extends ServiceBaseException {

    private static final String MESSAGE = "Auction is expired";

    public AuctionNotAvailableException() {
        super(MESSAGE);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_ACCEPTABLE;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.NOT_ACCEPTABLE;
    }
}
