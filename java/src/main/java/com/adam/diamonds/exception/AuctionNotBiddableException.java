package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class AuctionNotBiddableException extends ServiceBaseException {

    private static final String MESSAGE = "Auction is not biddable";

    public AuctionNotBiddableException() {
        super(MESSAGE);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.GONE;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.GONE;
    }

}
