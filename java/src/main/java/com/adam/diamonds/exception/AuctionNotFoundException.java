package com.adam.diamonds.exception;

public class AuctionNotFoundException extends NotFoundException {

    private static final String MESSAGE = "Auction not found.";

    public AuctionNotFoundException() {
        super(MESSAGE);
    }
}
