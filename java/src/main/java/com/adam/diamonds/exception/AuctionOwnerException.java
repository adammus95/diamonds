package com.adam.diamonds.exception;

public class AuctionOwnerException extends NotAcceptableException {

    private static final String MESSAGE = "You are owner of this auction";

    public AuctionOwnerException() {
        super(MESSAGE);
    }
}
