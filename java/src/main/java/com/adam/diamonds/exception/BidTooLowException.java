package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class BidTooLowException extends ServiceBaseException {


    private static final String MESSAGE = "Bid is too low";

    public BidTooLowException() {
        super(MESSAGE);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_ACCEPTABLE;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.NOT_ACCEPTABLE;
    }

}
