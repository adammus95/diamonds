package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class EmailExistException extends ServiceBaseException {


    private static final String MESSAGE = "Email already exist";

    public EmailExistException () {
        super(MESSAGE);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_ACCEPTABLE;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.NOT_ACCEPTABLE;
    }
}
