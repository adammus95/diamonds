package com.adam.diamonds.exception;

public class ItemNotFoundException extends NotFoundException{

    private static final String MESSAGE = "Item not found.";

    public ItemNotFoundException() {
        super(MESSAGE);
    }
}
