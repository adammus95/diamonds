package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class ItemOnAuctionException extends ServiceBaseException {

    private static final String MESSAGE = "Item is already on auction";

    public ItemOnAuctionException() {
        super(MESSAGE);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.NOT_FOUND;
    }
}
