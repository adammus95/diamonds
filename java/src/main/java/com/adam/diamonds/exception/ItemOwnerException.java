package com.adam.diamonds.exception;

public class ItemOwnerException extends NotAcceptableException {

    private static final String MESSAGE = "You are not an owner of this items";

    public ItemOwnerException() { super(MESSAGE); }
}
