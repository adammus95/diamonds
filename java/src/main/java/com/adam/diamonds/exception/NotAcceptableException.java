package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class NotAcceptableException extends ServiceBaseException {

    public NotAcceptableException(String message) {
        super(message);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_ACCEPTABLE;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.NOT_ACCEPTABLE;
    }


}
