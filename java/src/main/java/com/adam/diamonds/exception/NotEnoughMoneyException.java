package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class NotEnoughMoneyException extends ServiceBaseException {

    private static final String MESSAGE = "Not enough money";

    public NotEnoughMoneyException() {
        super(MESSAGE);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_ACCEPTABLE;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.NOT_ACCEPTABLE;
    }

}
