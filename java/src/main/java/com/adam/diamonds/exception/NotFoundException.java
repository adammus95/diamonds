package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends ServiceBaseException {

    public NotFoundException(String message) {
        super(message);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.NOT_FOUND;
    }

}
