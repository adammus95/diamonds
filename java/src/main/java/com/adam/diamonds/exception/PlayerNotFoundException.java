package com.adam.diamonds.exception;


public class PlayerNotFoundException extends NotFoundException {
    private static final String MESSAGE = "User not found.";

    public PlayerNotFoundException() {
        super(MESSAGE);
    }
}
