package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public abstract class ServiceBaseException extends RuntimeException {

    public ServiceBaseException(String message) {
        super(message);
    }

    public UnsucessfulResponse toUnsuccessfulResponse() {
        return new UnsucessfulResponse(getCode().code(), getMessage(), getHttpStatusCode());

    }

    protected abstract HttpStatus getHttpStatusCode();

    protected abstract ValidationCode getCode();
}
