package com.adam.diamonds.exception;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class UnsucessfulResponse {
    private Integer code;

    private String message;

    @JsonIgnore
    private HttpStatus httpStatus;

}
