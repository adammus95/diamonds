package com.adam.diamonds.exception;

import org.springframework.http.HttpStatus;

public class UserExistException extends ServiceBaseException {

    private static final String MESSAGE = "Username already exist";

    public UserExistException () {
        super(MESSAGE);
    }

    @Override
    protected HttpStatus getHttpStatusCode() {
        return HttpStatus.NOT_ACCEPTABLE;
    }

    @Override
    protected ValidationCode getCode() {
        return ValidationCode.NOT_ACCEPTABLE;
    }
}
