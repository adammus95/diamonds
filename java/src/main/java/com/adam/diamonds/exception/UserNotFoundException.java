package com.adam.diamonds.exception;

public class UserNotFoundException extends NotFoundException {

    private static final String MESSAGE = "User not found.";

    public UserNotFoundException(){
        super(MESSAGE);
    }

}
