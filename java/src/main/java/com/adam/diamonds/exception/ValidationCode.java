package com.adam.diamonds.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Getter
@Accessors(fluent = true)
@RequiredArgsConstructor
public enum ValidationCode {
    NOT_FOUND(404, "NOT_FOUND_EXCEPTION"),
    BAD_REQUEST(400, "BAD_REQUEST"),
    NOT_ACCEPTABLE(406, "NOT_ACCEPTABLE"),
    GONE(410, "GONE");

    private final int code;
    private final String message;

}
