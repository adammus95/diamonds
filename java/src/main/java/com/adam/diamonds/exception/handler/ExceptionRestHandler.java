package com.adam.diamonds.exception.handler;

import com.adam.diamonds.exception.ServiceBaseException;
import com.adam.diamonds.exception.UnsucessfulResponse;
import com.adam.diamonds.exception.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionRestHandler {

    @ExceptionHandler(ServiceBaseException.class)
    public ResponseEntity<UnsucessfulResponse> responseExceptionHandler(ServiceBaseException ex){
        final UnsucessfulResponse response = ex.toUnsuccessfulResponse();

        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<UnsucessfulResponse> responseExceptionHandler(){
        final UnsucessfulResponse response = new UserNotFoundException().toUnsuccessfulResponse();

        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<UnsucessfulResponse> responseExceptionHandler2(final MethodArgumentNotValidException ex){

        String message = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();

        final UnsucessfulResponse response = new UnsucessfulResponse().setCode(400).setMessage(message).setHttpStatus(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<UnsucessfulResponse> responseExceptionHandler3(final BindException ex){

        String message = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();

        final UnsucessfulResponse response = new UnsucessfulResponse().setCode(400).setMessage(message).setHttpStatus(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(response, response.getHttpStatus());
    }

}
