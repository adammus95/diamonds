package com.adam.diamonds.mapper;

import org.modelmapper.ModelMapper;

public class DTOMapper {

    public static <T, F> F map(T t, Class<F> f) {
        final ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(t, f);
    }
}
