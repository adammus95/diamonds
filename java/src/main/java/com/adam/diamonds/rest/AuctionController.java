package com.adam.diamonds.rest;


import com.adam.diamonds.dto.AuctionDTO;
import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.rest.requests.AddAuctionRequest;
import com.adam.diamonds.rest.requests.AuctionsRequestParams;
import com.adam.diamonds.rest.responses.AuctionResponse;
import com.adam.diamonds.service.AuctionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/auction")
@RequiredArgsConstructor
public class AuctionController {

    private final AuctionService auctionService;

    @PostMapping
    ResponseEntity<AuctionDTO> add(@RequestBody @Valid AddAuctionRequest auctionRequest, Principal principal){
        return ResponseEntity.ok(auctionService.add(auctionRequest, principal));
    }

    @PostMapping("/{id}")
    ResponseEntity<List<ItemStoneDTO>> buy(@PathVariable @NotNull Long id, Principal principal){
        return ResponseEntity.ok(auctionService.buy(id, principal));
    }

    @PutMapping("/{id}")
    ResponseEntity<AuctionDTO> get(@PathVariable @NotNull Long id){
        return ResponseEntity.ok((auctionService.getAuction(id)));
    }

    @GetMapping
    ResponseEntity<AuctionResponse> getActiveAuctions(@Valid AuctionsRequestParams auctionsRequestParams){
        return ResponseEntity.ok(auctionService.getActiveAuctions(auctionsRequestParams));
    }

}
