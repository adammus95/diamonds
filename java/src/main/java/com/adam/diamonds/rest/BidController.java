package com.adam.diamonds.rest;


import com.adam.diamonds.dto.BidDTO;
import com.adam.diamonds.rest.requests.BidRequest;
import com.adam.diamonds.service.BidService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/bid")
@RequiredArgsConstructor
public class BidController {

    private final BidService bidService;

    @PostMapping()
    ResponseEntity<BidDTO> bid(@RequestBody BidRequest bidRequest, Principal principal){
        return ResponseEntity.ok(bidService.bid(bidRequest, principal));
    }


}
