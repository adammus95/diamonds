package com.adam.diamonds.rest;

import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.rest.requests.ContractRequest;
import com.adam.diamonds.service.ContractService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/contract")
@RequiredArgsConstructor
public class ContractController {


    private final ContractService contractService;

    @PostMapping("")
    public ResponseEntity<ItemStoneDTO> signContract(@RequestBody @Valid ContractRequest contractRequest, Principal principal) {
        return ResponseEntity.ok(contractService.signContract(contractRequest.getItemStones(), principal));
    }

}
