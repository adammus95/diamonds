package com.adam.diamonds.rest;

import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.rest.requests.CutRequest;
import com.adam.diamonds.service.CutService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/cut")
@RequiredArgsConstructor
public class CutController {


    private final CutService cutService;

    @PostMapping("")
    ResponseEntity<ItemStoneDTO> cut(Principal principal, @RequestBody CutRequest cutRequest){
        return ResponseEntity.ok(cutService.cut(principal, cutRequest));
    }

}
