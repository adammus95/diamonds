package com.adam.diamonds.rest;

import com.adam.diamonds.dto.DrawDTO;
import com.adam.diamonds.service.DrawService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/draw")
@RequiredArgsConstructor
public class DrawController {

    private final DrawService drawService;

    @GetMapping()
    public ResponseEntity<DrawDTO> getLastDraw(Principal principal) {
        return ResponseEntity.ok(drawService.getLastDraw(principal));
    }
    @GetMapping("/last")
    public ResponseEntity<List<DrawDTO>> getLastDraws() {
        return ResponseEntity.ok(drawService.getLastDraws());
    }

}
