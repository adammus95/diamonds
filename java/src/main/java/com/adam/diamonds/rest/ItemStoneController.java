package com.adam.diamonds.rest;


import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.rest.responses.ItemStoneResponse;
import com.adam.diamonds.service.ItemStoneService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/item")
@RequiredArgsConstructor
public class ItemStoneController {

    private final ItemStoneService itemStoneService;

    @GetMapping("/{username}")
    ResponseEntity<ItemStoneResponse> getItems(@PathVariable @NotNull String username, @RequestParam Integer page, Integer size, Optional<String> sort, Optional<Sort.Direction> direction){

        return ResponseEntity.ok(itemStoneService.findAllByUsername(username, PageRequest.of(page, size, Sort.by(direction.orElse(Sort.Direction.DESC), sort.orElse("weight")))));
    }

    @PostMapping("/draw")
    ResponseEntity<ItemStoneDTO> draw(Principal principal){

        return ResponseEntity.ok(itemStoneService.draw(principal));
    }

    @GetMapping("/draw/ghost")
    public ResponseEntity<List<ItemStoneDTO>> getGhostItemStones(@RequestParam Integer size) {
        return ResponseEntity.ok(itemStoneService.drawGhostItemStones(size));
    }

}
