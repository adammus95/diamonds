package com.adam.diamonds.rest;

import com.adam.diamonds.dto.UserDTO;
import com.adam.diamonds.dto.UserDetailDTO;
import com.adam.diamonds.dto.UserInfoDTO;
import com.adam.diamonds.rest.requests.RegisterRequest;
import com.adam.diamonds.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getUsers() {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/search")
    public ResponseEntity<List<UserInfoDTO>> searchUsers(@RequestParam String partUsername) {
        return ResponseEntity.ok(userService.findByPartUsername(partUsername));
    };

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable @NotNull Long id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> createUser(@RequestBody @Valid RegisterRequest registerRequest) {
        return ResponseEntity.ok(userService.createUser(registerRequest));
    }

    @GetMapping("/details")
    public ResponseEntity<UserDetailDTO> getUserMoney(Principal principal) {
        return ResponseEntity.ok(userService.getUserDetails(principal));
    }

    @GetMapping("/name/{username}")
    public ResponseEntity<UserDTO> getUser(@PathVariable @NotNull String username) {
        return ResponseEntity.ok(userService.findByUsername(username));
    }

}
