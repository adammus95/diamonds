package com.adam.diamonds.rest.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class AddAuctionRequest {

    @NotNull(message = "Price can't be null")
    @DecimalMin(value = "0.01", message = "Price must be greater than 0.01")
    @DecimalMax(value = "1000000000", message = "Price must be lower than 1000000000")
    private BigDecimal price;

    private boolean biddable;

    @NotNull
    @Size(min = 1, max = 15, message = "Number of items must be greater than 1 and lower than 15")
    private List<Long> itemStones;

}
