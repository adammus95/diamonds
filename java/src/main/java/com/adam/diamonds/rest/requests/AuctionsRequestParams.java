package com.adam.diamonds.rest.requests;

import com.adam.diamonds.enums.Cut;
import com.adam.diamonds.enums.Stone;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class AuctionsRequestParams {

    @NotNull
    private Integer page;

    @NotNull
    private Integer size;

    @Nullable
    private Optional<String> sort;

    @Nullable
    private Optional<Sort.Direction> direction;

    @Nullable
    private BigDecimal minPrice;

    @Nullable
    private BigDecimal maxPrice;

    @Nullable
    private Double minWeight;

    @Nullable
    private Double maxWeight;

    @Nullable
    private Integer minCondition;

    @Nullable
    private Integer maxCondition;

    @Nullable
    private List<Cut> cuts;

    @Nullable
    private List<Stone> stones;

}
