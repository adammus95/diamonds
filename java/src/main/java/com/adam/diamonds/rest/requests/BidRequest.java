package com.adam.diamonds.rest.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class BidRequest {

    @NotNull
    private Long id;

    @NotNull
    @DecimalMin(value = "0.01", message = "Bid must be greater than 0.01")
    @DecimalMax(value = "1000000000", message = "Bid must be lower than 1000000000")
    private BigDecimal price;

}
