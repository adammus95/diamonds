package com.adam.diamonds.rest.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ContractRequest {

    @NotNull
    @Size(min = 5, max = 5)
    private List<Long> itemStones;
}
