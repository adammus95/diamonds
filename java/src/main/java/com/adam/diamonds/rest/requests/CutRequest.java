package com.adam.diamonds.rest.requests;

import com.adam.diamonds.enums.Cut;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class CutRequest {

    @NotNull
    private Long id;
    @NotNull
    private Cut cut;
    @NotNull
    @Size(min = 10, max = 100)
    private Integer percent;
}
