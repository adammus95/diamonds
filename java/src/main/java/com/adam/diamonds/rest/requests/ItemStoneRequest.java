package com.adam.diamonds.rest.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ItemStoneRequest {

    private int page;
    private int size;

}
