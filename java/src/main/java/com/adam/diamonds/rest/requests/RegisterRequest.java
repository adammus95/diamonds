package com.adam.diamonds.rest.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class RegisterRequest {

    @NotBlank(message = "email can't be empty")
    @Email(message = "incorrect email")
    private String email;

    @NotBlank(message = "password can't be empty")
    @Size(min = 6, max = 30, message = "password must be longer than 6 and shorter than 30 digits")
    private String password;

    @NotBlank(message = "username can't be empty")
    @Size(min = 4, max = 16, message = "username must be longer than 4 and shorter than 16 digits")
    @Pattern(regexp = "^[a-z0-9]+$")
    private String username;

}
