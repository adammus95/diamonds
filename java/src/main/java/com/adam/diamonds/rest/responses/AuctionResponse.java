package com.adam.diamonds.rest.responses;

import com.adam.diamonds.dto.AuctionDTO;
import com.adam.diamonds.dto.ItemStoneDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class AuctionResponse {

    private Long count;
    private List<AuctionDTO> auctions;
}
