package com.adam.diamonds.rest.responses;

import com.adam.diamonds.dto.UserDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;


@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class AuthenticationResponse {

    private UserDTO user;
    private String token;

}
