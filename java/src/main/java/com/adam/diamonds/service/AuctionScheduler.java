package com.adam.diamonds.service;


import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class AuctionScheduler {
    private final AuctionService auctionService;

    @Scheduled(fixedRateString = "PT5S")
    void endAuctions(){
        auctionService.closeExpiredAuctions();
    }

}