package com.adam.diamonds.service;

import com.adam.diamonds.dto.AuctionDTO;
import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.entity.Auction;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.rest.requests.AddAuctionRequest;
import com.adam.diamonds.rest.requests.AuctionsRequestParams;
import com.adam.diamonds.rest.responses.AuctionResponse;

import java.security.Principal;
import java.util.List;

public interface AuctionService {

    AuctionDTO add(AddAuctionRequest auctionRequest, Principal principal);
    AuctionDTO getAuction(Long id);
    Auction get(Long id);
    AuctionResponse getActiveAuctions(AuctionsRequestParams auctionsRequestParams);
    List<ItemStoneDTO> buy(Long id, Principal principal);
    void canUserBuy(User buyer, Auction auction);
    void closeExpiredAuctions();
}
