package com.adam.diamonds.service;

import com.adam.diamonds.dao.AuctionRepository;
import com.adam.diamonds.dto.AuctionDTO;
import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.entity.Auction;
import com.adam.diamonds.entity.Bid;
import com.adam.diamonds.entity.ItemStone;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.exception.AuctionNotAvailableException;
import com.adam.diamonds.exception.AuctionNotFoundException;
import com.adam.diamonds.exception.AuctionOwnerException;
import com.adam.diamonds.exception.NotEnoughMoneyException;
import com.adam.diamonds.mapper.DTOMapper;
import com.adam.diamonds.rest.requests.AddAuctionRequest;
import com.adam.diamonds.rest.requests.AuctionsRequestParams;
import com.adam.diamonds.rest.responses.AuctionResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuctionServiceImpl implements AuctionService {

    private final AuctionRepository auctionRepository;
    private final ItemStoneService itemStoneService;
    private final UserService userService;
    private final static BigDecimal FEE = new BigDecimal(0.95);
    private final SimpMessagingTemplate template;

    @Override
    public AuctionDTO add(AddAuctionRequest auctionRequest, Principal principal) {

        Auction auction = new Auction();
        User user = userService.findUserByUsername(principal.getName());

        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        LocalDateTime expired = now.plusDays(2);

        List<ItemStone> itemStones = itemStoneService.getNotAuctionedItemStonesOwnedByUser(user, auctionRequest.getItemStones());

        auction.setAddTime(now)
                .setExpirationTime(expired)
                .setItemStones(itemStones)
                .setBiddable(auctionRequest.isBiddable())
                .setPrice(auctionRequest.getPrice())
                .setUser(user)
                .setViews(0);

        itemStones.forEach(i -> {
            i.getAuctions().add(auction);
            i.setAvailable(false);
        });

        auctionRepository.save(auction);

        return DTOMapper.map(auction, AuctionDTO.class);
    }

    @Override
    public AuctionDTO getAuction(Long id) {
        Auction auction = auctionRepository.findById(id).orElseThrow(AuctionNotFoundException::new);
        auction.setViews(auction.getViews() + 1);
        auctionRepository.save(auction);
        return DTOMapper.map(auction, AuctionDTO.class);
    }

    @Override
    public Auction get(Long id) {
        return auctionRepository.findById(id).orElseThrow(AuctionNotFoundException::new);
    }

    @Override
    public AuctionResponse getActiveAuctions(AuctionsRequestParams auctionsRequestParams) {

        PageRequest pageRequest = PageRequest.of(
                auctionsRequestParams.getPage(),
                auctionsRequestParams.getSize(),
                Sort.by(auctionsRequestParams.getDirection().orElse(Sort.Direction.DESC),
                        auctionsRequestParams.getSort().orElse("addTime")));

        Page<Auction> auctionsPaged = auctionRepository.findAll(auctionsRequestParams, pageRequest);

        List<AuctionDTO> auctions = auctionsPaged.getContent().stream().map(x -> DTOMapper.map(x, AuctionDTO.class)).collect(Collectors.toList());

        return new AuctionResponse().setCount(auctionsPaged.getTotalElements()).setAuctions(auctions);
    }

    @Override
    public List<ItemStoneDTO> buy(Long id, Principal principal) {
        Auction auction = auctionRepository.findById(id).orElseThrow(AuctionNotFoundException::new);
        User buyer = userService.findUserByUsername(principal.getName());
        User seller = auction.getUser();

        canUserBuy(buyer, auction);

        BigDecimal price = auction.getPrice();
        buyer.getUserDetail().setMoney(buyer.getUserDetail().getMoney().subtract(price));
        seller.getUserDetail().setMoney(seller.getUserDetail().getMoney().add(price.multiply(FEE)));

        List<ItemStone> auctionedItemStones = auction.getItemStones();
        auctionedItemStones.forEach(itemStone -> {
            itemStone.setUser(buyer);
            itemStone.setAvailable(true);
        });
        auction.setRemoveTime(LocalDateTime.now(ZoneOffset.UTC));
        auction.setRemoved(true);

        auctionRepository.save(auction);

        return auctionedItemStones.stream()
                .map(x -> DTOMapper.map(x, ItemStoneDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void canUserBuy(User user, Auction auction) {

        if (user.getId().equals(auction.getUser().getId())) {
            throw new AuctionOwnerException();
        }
        if (user.getUserDetail().getMoney().compareTo(auction.getPrice()) < 0) {
            throw new NotEnoughMoneyException();
        }
        if (auction.isRemoved() || auction.getExpirationTime().compareTo(LocalDateTime.now(ZoneOffset.UTC)) < 0) {
            throw new AuctionNotAvailableException();
        }
    }

    @Transactional
    public void closeExpiredAuctions() {
        List<Auction> auctions = auctionRepository.findAllByRemovedFalseAndExpirationTimeBefore(LocalDateTime.now(ZoneOffset.UTC));
        auctions.forEach(auction -> {
                    List<Bid> bids = auction.getBids();

                    if (bids != null) {
                        bids.forEach(bid -> {
                            if (bid.isHighest()) {
                                auction.getItemStones().forEach(itemStone -> {
                                    itemStone.setUser(bid.getUser());
                                    itemStone.setAvailable(true);
                                });
                            } else if (bid.isCurrent()) {
                                bid.getUser().getUserDetail().setMoney(bid.getUser().getUserDetail().getMoney().add(bid.getPrice()));
                            }
                        });
                    }
                    auction.setRemoved(true)
                            .setRemoveTime(LocalDateTime.now(ZoneOffset.UTC))
                            .getItemStones().forEach(itemStone -> {
                        itemStone.setAvailable(true);
                    });
                    auctionRepository.saveAndFlush(auction);
                }
        );
        if (auctions.size() > 0) {
            template.convertAndSend("/topic/auction/post", "");
        }
    }


}
