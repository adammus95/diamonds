package com.adam.diamonds.service;

import com.adam.diamonds.rest.requests.AuthenticationRequest;
import com.adam.diamonds.rest.responses.AuthenticationResponse;

public interface AuthenticateService {

    AuthenticationResponse login(AuthenticationRequest authenticationRequest);

}
