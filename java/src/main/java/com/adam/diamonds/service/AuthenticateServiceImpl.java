package com.adam.diamonds.service;

import com.adam.diamonds.dto.UserDTO;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.jwt.JwtUtil;
import com.adam.diamonds.mapper.DTOMapper;
import com.adam.diamonds.rest.requests.AuthenticationRequest;
import com.adam.diamonds.rest.responses.AuthenticationResponse;
import com.adam.diamonds.security.MyUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class AuthenticateServiceImpl implements AuthenticateService {

    private final AuthenticationManager authenticationManager;

    private final MyUserDetailsService userDetailsService;

    private final UserService userService;

    private final JwtUtil jwtTokenUtil;

    @Override
    public AuthenticationResponse login(AuthenticationRequest authenticationRequest) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
        );

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String jwt = jwtTokenUtil.generateToken(userDetails);

        final User user = userService.findUserByUsername(authenticationRequest.getUsername());

        return new AuthenticationResponse().setToken(jwt).setUser(DTOMapper.map(user, UserDTO.class));

    }
}
