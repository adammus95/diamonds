package com.adam.diamonds.service;


import com.adam.diamonds.dto.BidDTO;
import com.adam.diamonds.rest.requests.BidRequest;

import java.security.Principal;
import java.util.List;

public interface BidService {

    BidDTO bid(BidRequest bidRequest, Principal principal);

}
