package com.adam.diamonds.service;

import com.adam.diamonds.dao.BidRepository;
import com.adam.diamonds.dto.BidDTO;
import com.adam.diamonds.entity.Auction;
import com.adam.diamonds.entity.Bid;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.exception.AuctionNotBiddableException;
import com.adam.diamonds.exception.BidTooLowException;
import com.adam.diamonds.exception.NotEnoughMoneyException;
import com.adam.diamonds.mapper.DTOMapper;
import com.adam.diamonds.rest.requests.BidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
@RequiredArgsConstructor
public class BidServiceImpl implements BidService {

    private final AuctionService auctionService;
    private final UserService userService;
    private final BidRepository bidRepository;

    @Override
    public BidDTO bid(BidRequest bidRequest, Principal principal) {

        Auction auction = auctionService.get(bidRequest.getId());

        if (!auction.isBiddable()){
            throw new AuctionNotBiddableException();
        }

        User user = userService.findUserByUsername(principal.getName());
        Bid highestBid = bidRepository.findByAuctionAndHighestTrue(auction);

        if(highestBid != null){
            if(bidRequest.getPrice().compareTo(highestBid.getPrice()) < 1){
                throw new BidTooLowException();
            }
            if (user.getUserDetail().getMoney().compareTo(highestBid.getPrice()) < 0) {
                throw new NotEnoughMoneyException();
            }
            highestBid.setHighest(false);
        }else if(bidRequest.getPrice().compareTo(auction.getPrice()) < 1){
                throw new BidTooLowException();
        }

        if (user.getUserDetail().getMoney().compareTo(bidRequest.getPrice()) < 0) {
            throw new NotEnoughMoneyException();
        }

        auctionService.canUserBuy(user, auction);

        Bid lastUserBid = bidRepository.findByAuctionAndUserAndCurrentTrue(auction, user);
        if(lastUserBid != null){
            lastUserBid.setCurrent(false);
            user.getUserDetail().setMoney(user.getUserDetail().getMoney().add(lastUserBid.getPrice().subtract(bidRequest.getPrice())));
        }else {
            user.getUserDetail().setMoney(user.getUserDetail().getMoney().subtract(bidRequest.getPrice()));
        }

        Bid bid = new Bid();
        bid.setAddTime(LocalDateTime.now(ZoneOffset.UTC))
                .setHighest(true)
                .setAuction(auction)
                .setCurrent(true)
                .setUser(user)
                .setPrice(bidRequest.getPrice());

        bidRepository.save(bid);

        //return auction.getBids().stream().map(x -> DTOMapper.map(x, BidDTO.class)).collect(Collectors.toList());
        return DTOMapper.map(bid, BidDTO.class);
    }
}
