package com.adam.diamonds.service;

import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.entity.ItemStone;

import java.security.Principal;
import java.util.List;

public interface ContractService {

    ItemStoneDTO signContract(List<Long> ids, Principal principal);

}
