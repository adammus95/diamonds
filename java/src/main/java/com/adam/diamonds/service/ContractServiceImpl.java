package com.adam.diamonds.service;

import com.adam.diamonds.dao.ItemStoneRepository;
import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.entity.ItemStone;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.mapper.DTOMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class ContractServiceImpl implements ContractService {

    private final UserService userService;
    private final ItemStoneService itemStoneService;
    private final ItemStoneRepository itemStoneRepository;

    @Override
    public ItemStoneDTO signContract(List<Long> ids, Principal principal) {

        User user = userService.findUserByUsername(principal.getName());

        List<ItemStone> itemStones = itemStoneService.getNotAuctionedItemStonesOwnedByUser(user, ids);

        Random random = new Random();
        Integer winIndex = random.nextInt(5);

        itemStones.forEach(itemStone -> {
            itemStone.setRemoved(true);
        });

        itemStones.get(winIndex).setRemoved(false).setWeight(itemStones.get(winIndex).getWeight() * 2);

        itemStoneRepository.saveAll(itemStones);

        return DTOMapper.map(itemStones.get(winIndex), ItemStoneDTO.class);
    }
}
