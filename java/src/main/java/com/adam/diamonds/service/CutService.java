package com.adam.diamonds.service;

import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.enums.Cut;
import com.adam.diamonds.rest.requests.CutRequest;

import java.security.Principal;

public interface CutService {

    ItemStoneDTO cut(Principal principal, CutRequest cutRequest);

}
