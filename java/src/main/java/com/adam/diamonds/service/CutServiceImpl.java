package com.adam.diamonds.service;

import com.adam.diamonds.dao.ItemStoneRepository;
import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.entity.ItemStone;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.mapper.DTOMapper;
import com.adam.diamonds.rest.requests.CutRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Arrays;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class CutServiceImpl implements CutService {

    private final UserService userService;
    private final ItemStoneService itemStoneService;
    private final ItemStoneRepository itemStoneRepository;

    @Override
    public ItemStoneDTO cut(Principal principal, CutRequest cutRequest) {

        User user = userService.findUserByUsername(principal.getName());

        ItemStone itemStone = this.itemStoneService.getNotAuctionedItemStonesOwnedByUser(user, Arrays.asList(cutRequest.getId())).get(0);

        Integer chance = 110 - cutRequest.getPercent();
        Random rand = new Random();
        Integer luckyNumber = rand.nextInt(100) + 1;
        if (luckyNumber <= chance) {
            itemStone.setCut(cutRequest.getCut());
            double finalWeight = Math.round(itemStone.getWeight() * cutRequest.getPercent()) / 100.0;
            finalWeight = finalWeight < 0.01 ? 0.01 : finalWeight;
            itemStone.setWeight(finalWeight);
        } else {
            itemStone.setRemoved(true);
        }

        this.itemStoneRepository.save(itemStone);

        return DTOMapper.map(itemStone, ItemStoneDTO.class);

    }
}
