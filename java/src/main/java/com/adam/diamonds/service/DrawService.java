package com.adam.diamonds.service;

import com.adam.diamonds.dto.DrawDTO;

import java.security.Principal;
import java.util.List;

public interface DrawService {

    DrawDTO getLastDraw(Principal principal);
    List<DrawDTO> getLastDraws();
}
