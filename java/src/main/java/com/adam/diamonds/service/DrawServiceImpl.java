package com.adam.diamonds.service;

import com.adam.diamonds.dao.DrawRepository;
import com.adam.diamonds.dto.DrawDTO;
import com.adam.diamonds.entity.Draw;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.mapper.DTOMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DrawServiceImpl implements DrawService {

    private final DrawRepository drawRepository;
    private final UserService userService;


    @Override
    public DrawDTO getLastDraw(Principal principal) {
        User user = userService.findUserByUsername(principal.getName());
        Draw draw = drawRepository.findTopByUserDetailOrderByDrawTimeDesc(user.getUserDetail());
        if (draw != null) {
            return DTOMapper.map(draw, DrawDTO.class);
        }
        return null;

    }

    @Override
    public List<DrawDTO> getLastDraws() {
        List<Draw> draws = drawRepository.findTop10ByDrawTimeBeforeOrderByDrawTimeDesc(LocalDateTime.now(ZoneOffset.UTC).minusSeconds(8));
        return draws.stream().map(x -> DTOMapper.map(x, DrawDTO.class)).collect(Collectors.toList());
    }
}
