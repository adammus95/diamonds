package com.adam.diamonds.service;

import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.entity.ItemStone;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.rest.responses.ItemStoneResponse;
import org.springframework.data.domain.PageRequest;

import java.security.Principal;
import java.util.List;

public interface ItemStoneService {

    ItemStoneResponse findAllByUsername(String username, PageRequest pageRequest);
    ItemStoneDTO draw(Principal principal);
    List<ItemStoneDTO> drawGhostItemStones(Integer size);
    List<ItemStone> getNotAuctionedItemStonesOwnedByUser(User user, List<Long> id);

}
