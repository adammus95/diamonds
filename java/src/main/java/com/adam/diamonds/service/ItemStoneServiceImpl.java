package com.adam.diamonds.service;

import com.adam.diamonds.dao.AuctionRepository;
import com.adam.diamonds.dao.DrawRepository;
import com.adam.diamonds.dao.ItemStoneRepository;
import com.adam.diamonds.dao.UserRepository;
import com.adam.diamonds.dto.ItemStoneDTO;
import com.adam.diamonds.entity.Auction;
import com.adam.diamonds.entity.Draw;
import com.adam.diamonds.entity.ItemStone;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.enums.Cut;
import com.adam.diamonds.enums.Stone;
import com.adam.diamonds.exception.ItemNotFoundException;
import com.adam.diamonds.exception.ItemOnAuctionException;
import com.adam.diamonds.exception.ItemOwnerException;
import com.adam.diamonds.exception.NotAcceptableException;
import com.adam.diamonds.mapper.DTOMapper;
import com.adam.diamonds.rest.responses.ItemStoneResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ItemStoneServiceImpl implements ItemStoneService {

    private final ItemStoneRepository itemStoneRepository;
    private final AuctionRepository auctionRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final DrawRepository drawRepository;
    private final SimpMessagingTemplate template;
    private TaskScheduler scheduler;

    @Override
    public ItemStoneResponse findAllByUsername(String username, PageRequest pageRequest) {

        Page<ItemStone> itemStonesPaged = itemStoneRepository.findAllByUserAndRemovedFalse(userService.findUserByUsername(username), pageRequest);
        List<ItemStoneDTO> itemStones = itemStonesPaged.getContent().stream().map(x -> DTOMapper.map(x, ItemStoneDTO.class)).collect(Collectors.toList());

        return new ItemStoneResponse().setCount(itemStonesPaged.getTotalElements()).setItemStones(itemStones);
    }

    @Override
    public ItemStoneDTO draw(Principal principal) {

        User user = userService.findUserByUsername(principal.getName());
        Draw draw = drawRepository.findTopByUserDetailOrderByDrawTimeDesc(user.getUserDetail());

        if (draw != null && draw.getDrawTime().compareTo(LocalDateTime.now(ZoneOffset.UTC).minusSeconds(20)) > 0) {
            throw new NotAcceptableException("There is still cooldown");
        }

        ItemStone itemStone = randomStone(user);

        user.getUserDetail().getDraws().add(new Draw().setDrawTime(LocalDateTime.now(ZoneOffset.UTC)).setUserDetail(user.getUserDetail()).setItemStone(itemStone));

        userRepository.save(user);

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        scheduler = new ConcurrentTaskScheduler(executorService);
        scheduler.schedule(this::sendDrawWebsocketInfo, new Date().from(LocalDateTime.now(ZoneOffset.UTC).plusSeconds(8).toInstant(ZoneOffset.UTC)));

        return DTOMapper.map(itemStone, ItemStoneDTO.class);
    }

    public void sendDrawWebsocketInfo() {
        template.convertAndSend("/topic/draw/post", "");
    }

    @Override
    public List<ItemStoneDTO> drawGhostItemStones(Integer size) {
        if (size > 100) {
            throw new NotAcceptableException("Size is too big");
        }
        List<ItemStone> itemStones = new ArrayList<>();
        for (int i = 0; i < size; i++){
            itemStones.add(randomStone(null));
        }
        return itemStones.stream().map(x -> DTOMapper.map(x, ItemStoneDTO.class)).collect(Collectors.toList());
    }

    @Override
    public List<ItemStone> getNotAuctionedItemStonesOwnedByUser(User user, List<Long> id) {
        List<ItemStone> itemStones = itemStoneRepository.findAllById(id);
        if (id.size() < itemStones.size()){
            throw new ItemNotFoundException();
        }

        itemStones.forEach(item ->{
            List<Auction> auctions = auctionRepository.findAllByItemStonesAndRemovedFalse(item);
            if (auctions.size() > 0){
                throw new ItemOnAuctionException();
            }
            if(item.getUser().getId() != user.getId()){
                throw new ItemOwnerException();
            }
            if(item.isRemoved()) {
                throw new ItemNotFoundException();
            }
        });
        return itemStones;
    }

    public ItemStone randomStone(User user) {

        ItemStone itemStone = new ItemStone();
        Random rand = new Random();
        Stone stone = Stone.random();
        Double weight = Stone.randomWeight();
        Integer condition = rand.nextInt(100) + 1;
        Cut cut = Cut.randomNoCut();

        itemStone.setColor(Stone.randomColor(stone))
                .setWeight(weight)
                .setCondition(condition)
                .setCut(cut)
                .setStone(stone)
                .setUser(user);
        return itemStone;
    }

}
