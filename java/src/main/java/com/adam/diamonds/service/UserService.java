package com.adam.diamonds.service;

import com.adam.diamonds.dto.UserDTO;
import com.adam.diamonds.dto.UserDetailDTO;
import com.adam.diamonds.dto.UserInfoDTO;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.rest.requests.RegisterRequest;

import java.security.Principal;
import java.util.List;

public interface UserService {

    List<UserDTO> findAll();
    UserDTO findById(Long id);
    UserDTO findByUsername(String username);
    User findUserByUsername(String username);
    List<UserInfoDTO> findByPartUsername(String partUsername);
    UserDTO createUser(RegisterRequest registerRequest);

    UserDetailDTO getUserDetails(Principal principal);
}
