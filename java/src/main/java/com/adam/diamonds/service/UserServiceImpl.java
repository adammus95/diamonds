package com.adam.diamonds.service;

import com.adam.diamonds.dao.UserRepository;
import com.adam.diamonds.dto.UserDTO;
import com.adam.diamonds.dto.UserDetailDTO;
import com.adam.diamonds.dto.UserInfoDTO;
import com.adam.diamonds.entity.User;
import com.adam.diamonds.entity.UserDetail;
import com.adam.diamonds.exception.EmailExistException;
import com.adam.diamonds.exception.UserExistException;
import com.adam.diamonds.exception.UserNotFoundException;
import com.adam.diamonds.mapper.DTOMapper;
import com.adam.diamonds.rest.requests.RegisterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public List<UserDTO> findAll() {
        return userRepository.findAll().stream().map(x -> DTOMapper.map(x, UserDTO.class)).collect(Collectors.toList());
    }

    @Override
    public UserDTO findById(Long id) {
        return DTOMapper.map(userRepository.findById(id).orElseThrow(UserNotFoundException::new), UserDTO.class);

    }

    @Override
    public UserDTO findByUsername(String username) {
        return DTOMapper.map(userRepository.findByUsername(username).orElseThrow(UserNotFoundException::new), UserDTO.class);
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<UserInfoDTO> findByPartUsername(String partUsername) {
        return userRepository.findAllByUsernameContaining(partUsername).stream().map(x -> DTOMapper.map(x, UserInfoDTO.class)).collect(Collectors.toList());
    }

    @Override
    public UserDTO createUser(RegisterRequest registerRequest) {

        User user = new User();
        UserDetail userDetail = new UserDetail();

        if (userRepository.existsByUsername(registerRequest.getUsername())){
            throw new UserExistException();
        }
        if (userRepository.findByEmail(registerRequest.getEmail()).orElse(null) != null) {
            throw new EmailExistException();
        }

        userDetail.setPoints(0)
                .setLuck(0)
                .setLevel(0)
                .setMoney(new BigDecimal(1000))
                .setCooldownReduction(0);


        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(registerRequest.getPassword());

        user.setUsername(registerRequest.getUsername())
                .setPassword(hashedPassword)
                .setEmail(registerRequest.getEmail())
                .setUserDetail(userDetail);

        userDetail.setUser(user);

        userRepository.save(user);

        return DTOMapper.map(user, UserDTO.class);
    }

    @Override
    public UserDetailDTO getUserDetails(Principal principal) {
        return DTOMapper.map(userRepository.findByUsername(principal.getName()).orElseThrow(UserNotFoundException::new).getUserDetail(), UserDetailDTO.class);
    }
}
