package com.adam.diamonds.websocket;

import com.adam.diamonds.service.AuctionService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class WebSocketController {

        @MessageMapping("auctions")
        @SendTo("/topic/auction/post")
        public String  postAuction() {
                return "Fetch auctions";
        }

        @MessageMapping("draws")
        @SendTo("/topic/draw/post")
        public String  postDraw() {
                return "Fetch draws";
        }

}
