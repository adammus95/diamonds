package com.adam.diamonds.service

import com.adam.diamonds.dao.AuctionRepository
import com.adam.diamonds.dto.AuctionDTO
import com.adam.diamonds.entity.Auction
import com.adam.diamonds.entity.ItemStone
import com.adam.diamonds.rest.requests.AddAuctionRequest
import org.springframework.messaging.simp.SimpMessagingTemplate
import spock.lang.Specification

import java.security.Principal

class AuctionServiceImplTest extends Specification {

    private AuctionRepository auctionRepository
    private AuctionServiceImpl auctionService
    private ItemStoneService itemStoneService
    private UserService userService
    private SimpMessagingTemplate simpMessagingTemplate

    def setup() {

        auctionRepository = Mock()
        itemStoneService = Mock()
        userService = Mock()
        simpMessagingTemplate = Mock()
        auctionService = new AuctionServiceImpl(auctionRepository, itemStoneService, userService, simpMessagingTemplate)

    }


    void 'test addNew Auction'() {

        given:
        auctionRepository.save(_ as Auction) >> new Auction().setId(id)
        itemStoneService.getNotAuctionedItemStonesOwnedByUser(_, _) >> [new ItemStone().setId(1)]
        Principal principal = new Principal() {
            @Override
            String getName() {
                return 'username'
            }
        }

        AddAuctionRequest addAuctionRequest = new AddAuctionRequest()
                .setPrice(price)
                .setItemStones([1])

        when:
        AuctionDTO auctionDTO = auctionService.add(addAuctionRequest, principal)

        then:
        auctionDTO
        auctionDTO.getPrice() == price

        1 * auctionRepository.save(_)

        where:
        id | price
        1  | BigDecimal.valueOf(100)

    }


}
