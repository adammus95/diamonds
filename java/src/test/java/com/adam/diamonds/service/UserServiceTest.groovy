package com.adam.diamonds.service

import com.adam.diamonds.dao.UserRepository
import com.adam.diamonds.dto.UserDTO
import com.adam.diamonds.entity.User
import com.adam.diamonds.exception.UserNotFoundException
import spock.lang.Specification
import spock.lang.Unroll

class UserServiceTest extends Specification {
    private UserRepository userRepository

    private UserServiceImpl userService


    def setup() {
        userRepository = Mock()
        userService = new UserServiceImpl(userRepository)
    }


    @Unroll
    void 'test findById id: #id, username: #username, email: #email'() {
        given:
        User user = new User()
                .setId(id)
                .setUsername(username)
                .setEmail(email)

        userRepository.findById(_ as Long) >> Optional.of(user)

        when:
        UserDTO userDTO = userService.findById(id)

        then:
        userDTO
        userDTO.getId() == id
        userDTO.getUsername() == username
        userDTO.getEmail() == email

        where:
        id | username   | email
        1  | 'username' | 'email'
        2  | 'kek'      | 'ks'
    }

    @Unroll
    void 'test findById throw UserNotFoundException'() {
        given:
        userRepository.findById(_ as Long) >> Optional.ofNullable(null)

        when:
        userService.findById(1)

        then:
        thrown(UserNotFoundException.class)

    }


}
