import {Component, OnDestroy, OnInit} from '@angular/core';
import {timer} from 'rxjs';
import {AlertService} from '../shared/alert.service';
import {Alert} from './alert.model';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {

  public alerts: Alert[] = new Array();

  constructor(private alertService: AlertService) {
  }

  ngOnDestroy(): void {
    this.alerts.forEach(alert => alert.subscription.unsubscribe());
  }

  ngOnInit() {
    this.alertService.getMessage().subscribe((alert: Alert) => {
      this.alerts.push(alert);
      if (alert) {
        this.startTimer(alert);
      }
    });
  }

  startTimer(alert: Alert) {
    const timer2 = timer(0, 1000);
    alert.subscription = timer2.subscribe(t => {
      alert.time--;
      if (alert.time < 1) {
        alert.subscription.unsubscribe();
        this.alerts.shift();
      }
    });
  }


}
