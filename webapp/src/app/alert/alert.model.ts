import {AlertType} from './alert-type.enum';
import {Subscription} from 'rxjs';

export class Alert {
  constructor(public type: AlertType, public message: string, public time: number, public subscription: Subscription) {}
}
