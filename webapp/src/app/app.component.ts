import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoginService} from './login/login.service';
import {DataStorageService} from './shared/data-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(private loginService: LoginService, private dataStorage: DataStorageService) {
  }

  title = 'diamonds';

  ngOnDestroy(): void {
    this.dataStorage.user.unsubscribe();
  }

  ngOnInit(): void {
    this.loginService.autoLogin();
    this.dataStorage.user.subscribe(user => {
      localStorage.setItem('userData', JSON.stringify(user));
    });
  }

}
