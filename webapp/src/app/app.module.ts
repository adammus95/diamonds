import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing.module';
import {HeaderComponent} from './header/header.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {DropdownDirective} from './shared/dropdown.directive';
import {RegisterComponent} from './register/register.component';
import {LoadingSpinnerComponent} from './shared/loading-spinner/loading-spinner.component';
import {UserComponent} from './user/user.component';
import {AuctionsComponent} from './auctions/auctions.component';
import {AuthInterceptorService} from './login/auth-interceptor.service';
import {AuctionComponent} from './auctions/auction/auction.component';
import {ItemStoneComponent} from './items/item-stone/item-stone.component';
import {HoverClassDirective} from './shared/hover.directive';
import {NgxPaginationModule} from 'ngx-pagination';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule, MatSliderModule} from '@angular/material';
import {ItemsPopupComponent} from './items-popup/items-popup.component';
import {MagnifierDirective} from './shared/magnifier.directive';
import {ItemsComponent} from './items/items.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {AddAuctionComponent} from './auctions/add-auction/add-auction.component';
import {CarouselComponent} from './carousel/carousel.component';
import {AlertComponent} from './alert/alert.component';
import {ItemInfoDirective} from './shared/item-info-directive';
import {DrawsComponent} from './draws/draws.component';
import {CutStoneComponent} from './cut-stone/cut-stone.component';
import {NgxPopper} from 'angular-popper';
import {Ng5SliderModule} from 'ng5-slider';
import {ContractComponent} from './contract/contract.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DropdownDirective,
    ItemInfoDirective,
    HoverClassDirective,
    MagnifierDirective,
    LoadingSpinnerComponent,
    UserComponent,
    AuctionsComponent,
    AuctionComponent,
    UserDetailsComponent,
    ItemsComponent,
    ItemStoneComponent,
    ItemsPopupComponent,
    AddAuctionComponent,
    CarouselComponent,
    DrawsComponent,
    CutStoneComponent,
    ContractComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxPopper,
    NgxPaginationModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatSliderModule,
    Ng5SliderModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}],
  bootstrap: [AppComponent],
  entryComponents: [ItemsPopupComponent]
})
export class AppModule {
}
