import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {RegisterComponent} from './register/register.component';
import {UserComponent} from './user/user.component';
import {AuctionsComponent} from './auctions/auctions.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {AddAuctionComponent} from './auctions/add-auction/add-auction.component';
import {CutStoneComponent} from './cut-stone/cut-stone.component';
import {ContractComponent} from './contract/contract.component';
import {AuthGuardService} from './login/auth-guard.service';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'user/:id', component: UserComponent},
  {path: 'auction', component: AuctionsComponent},
  {path: 'auction/add', canActivate: [AuthGuardService], component: AddAuctionComponent},
  {path: 'user/:id/details', component: UserDetailsComponent},
  {path: 'cutstone', canActivate: [AuthGuardService], component: CutStoneComponent},
  {path: 'contract', canActivate: [AuthGuardService], component: ContractComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
