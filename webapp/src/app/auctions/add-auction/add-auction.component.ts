import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {ItemStone} from '../../items/item-stone.model';
import {ItemsComponent} from '../../items/items.component';
import {FormControl, FormGroup} from '@angular/forms';
import {RequestsService} from '../../shared/requests.service';
import {Router} from '@angular/router';
import {SocketClientService} from '../../websocket/socket-client.service';
import {AlertService} from '../../shared/alert.service';

@Component({
  selector: 'app-add-auction',
  templateUrl: './add-auction.component.html',
  styleUrls: ['./add-auction.component.css']
})
export class AddAuctionComponent implements OnInit {

  @ViewChild('userItems', {static: true}) userItems;

  public size = 15;
  public auctionEmptySpaces: number[];
  public itemStonesRemovedFromEq: ItemStone[] = new Array();
  public canvasSize = 100;

  public newAuctionForm: FormGroup;

  constructor(private alertService: AlertService, private requestsService: RequestsService, private router: Router, private socketClientService: SocketClientService) {
  }

  ngOnInit() {
    if (window.innerWidth < 1000 || window.outerWidth < 1000) {
      this.canvasSize = 50;
    }
    this.newAuctionForm = new FormGroup({price: new FormControl(), isBiddable: new FormControl()});
    this.auctionEmptySpaces = Array(this.size).fill(0).map((x, i) => i);
  }

  onEqItemRemoved(itemStone: ItemStone) {
    if (itemStone && this.itemStonesRemovedFromEq.length < this.size) {
      this.itemStonesRemovedFromEq.push(itemStone);
      this.auctionEmptySpaces.pop();
    }
  }

  onAuctionItemRemoved(itemStone: ItemStone) {
    if (itemStone) {
      this.userItems.restoreItem(itemStone);
    }
  }

  onAddAuction() {
    this.requestsService.addAuction(this.newAuctionForm.value.price, this.itemStonesRemovedFromEq.map(item => item.id), this.newAuctionForm.value.isBiddable).subscribe(response => {
      this.socketClientService.send('/app/auctions');
      this.router.navigate(['auction']);
      this.alertService.success('Added new auction');
    });
  }

}
