export class AuctionRequestParams {

  constructor(public size: number,
              public page: number,
              public sort: string,
              public direction: string,
              public cuts: string[],
              public stones: string[],
              public minWeight: number,
              public maxWeight: number,
              public minCondition: number,
              public maxCondition: number,
              public minPrice: number,
              public maxPrice: number) {

  }

}
