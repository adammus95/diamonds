import {ItemStone} from '../items/item-stone.model';
import {UserInfo} from '../user/user-info.model';
import {Bid} from './auction/bid.mode';

export class Auction {
  constructor(private _id: number,
              public views: number,
              public biddable: boolean,
              public expirationTime: string,
              public price: number,
              public user: UserInfo,
              public itemStones: ItemStone[],
              public bids: Bid[]) {}
  get id() {
    return this._id;
  }
}
