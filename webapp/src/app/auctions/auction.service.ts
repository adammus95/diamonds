import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Auction} from './auction.model';

@Injectable({providedIn: 'root'})
export class AuctionService {

  constructor(private http: HttpClient) {}

  private auctions: Auction[];

  setAuctions(auctions: Auction[]) {
    this.auctions = auctions;
  }

  getAuctions() {
    return this.auctions;
  }
}
