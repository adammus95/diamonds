import {Component, ElementRef, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Auction} from '../auction.model';
import {RequestsService} from '../../shared/requests.service';
import {DataStorageService} from '../../shared/data-storage.service';
import {ItemsPopupComponent} from '../../items-popup/items-popup.component';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {ItemStone} from '../../items/item-stone.model';
import {Bid} from './bid.mode';
import {UserDetails} from '../../user-details/user-details.model';
import {timer} from 'rxjs';
import {TimeService} from '../../shared/time.service';
import {ItemsPopupService} from '../../shared/items-popup-service';
import {AlertService} from '../../shared/alert.service';
import {SocketClientService} from '../../websocket/socket-client.service';
import {User} from '../../login/user.model';

@Component({
  selector: 'app-auction',
  templateUrl: './auction.component.html',
  styleUrls: ['./auction.component.css']
})
export class AuctionComponent implements OnInit, OnDestroy {

  public currentBid: Bid;
  public endTime: string;
  public endDate: string;
  public timerSubscription;
  public user: User;
  public timeLeft;
  public date;

  constructor(private timeService: TimeService,
              private requestsService: RequestsService,
              private dataStorage: DataStorageService,
              private itemPopupService: ItemsPopupService,
              private alertService: AlertService,
              private socketClientService: SocketClientService) {
  }

  @Output() auctionRemoved = new EventEmitter<null>();
  @Input() auction: Auction;
  @Input() auctionIndex: string;
  @ViewChild('bidInput', {static: false}) bidInput: ElementRef;

  ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  ngOnInit() {

    if (this.auction.biddable) {
      const currentBid = this.auction.bids.find(value => value.current);
      if (currentBid) {
        this.currentBid = currentBid;
      }
    }

    this.dataStorage.user.subscribe((user: User) => {
      this.user = user;
    });

    this.date = this.timeService.timestampToLocalTime(this.auction.expirationTime);
    this.endDate = this.date.toLocaleString();
    this.startTimer();

  }

  startTimer() {
    const source = timer(0, 1000);
    this.timerSubscription = source.subscribe(val => {
      this.timeLeft = Number(this.date.getTime() - new Date().getTime() - this.dataStorage.dateTimeOffset);
      // if (this.timeLeft < 0) {
      //   this.auctionRemoved.emit(null);
      // }
      this.endTime = this.timeService.getTime(this.timeLeft / 1000).formatedTime;
    });
  }

  buy() {
    this.requestsService.buy(this.auction.id).subscribe((response: ItemStone[]) => {
      this.socketClientService.send('/app/auctions');
      this.itemPopupService.openPopup({itemStones: response, size: 150});
      this.getMoney();
      this.auctionRemoved.emit(null);
      this.alertService.success('You bought new items');
    });
  }

  bid() {
    this.requestsService.bid(this.auction.id, this.bidInput.nativeElement.value).subscribe((response: Bid) => {
      this.socketClientService.send('/app/auctions');
      this.currentBid = response;
      this.getMoney();
      this.alertService.success('Bid placed');
    });

  }

  getMoney() {
    this.requestsService.getUserDetails().subscribe((response: UserDetails) => {
      const user = this.dataStorage.user.getValue();
      user.userDetail = response;
      this.dataStorage.user.next(user);
    });
  }

}
