import {UserInfo} from '../../user/user-info.model';

export class Bid {
  constructor(private _id: number,
              public current: boolean,
              public biddable: boolean,
              public price: number,
              public user: UserInfo) {}
  get id() {
    return this._id;
  }
}
