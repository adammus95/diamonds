import {Component, OnInit} from '@angular/core';
import {AuctionService} from './auction.service';
import {DataStorageService} from '../shared/data-storage.service';
import {Auction} from './auction.model';
import {RequestsService} from '../shared/requests.service';
import {FormControl, FormGroup} from '@angular/forms';
import {SocketClientService} from '../websocket/socket-client.service';
import {AuctionRequestParams} from './auction-request-params.model';
import {LabelType, Options} from 'ng5-slider';


@Component({
  selector: 'app-auctions',
  templateUrl: './auctions.component.html',
  styleUrls: ['./auctions.component.css']
})
export class AuctionsComponent implements OnInit {

  public p = 1;
  public size = 20;
  displayAuctions: Auction[];
  count: number;
  public desc = true;

  public minPriceValue = 0;
  public maxPriceValue = 9999;

  public minWeightValue = 0.01;
  public maxWeightValue = 10;

  public minConditionValue = 1;
  public maxConditionValue = 100;

  public priceFilterOptions: Options = {
    floor: 0,
    ceil: 9999,
    showOuterSelectionBars: true,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '$' + value;
        case LabelType.High:
          return '$' + value;
        default:
          return '$' + value;
      }
    }
  };
  public weightFilterOptions: Options = {
    floor: 0.01,
    ceil: 10,
    step: 0.01,
    showOuterSelectionBars: true,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + 'ct';
        case LabelType.High:
          return value + 'ct';
        default:
          return value + 'ct';
      }
    }
  };
  public conditionFilterOptions: Options = {
    floor: 1,
    ceil: 100,
    step: 1,
    showOuterSelectionBars: true,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + '%';
        case LabelType.High:
          return value + '%';
        default:
          return value + '%';
      }
    }
  };

  public sortByForm: FormGroup;

  constructor(private auctionService: AuctionService,
              private requestsService: RequestsService,
              private socketClientService: SocketClientService,
              private dataStorage: DataStorageService) {
  }

  onValueChange() {
    this.getAuctions();
  }

  ngOnInit(): void {

    this.dataStorage.auctions.subscribe(auctios => {
      this.displayAuctions = auctios;
    });

    this.socketClientService.onMessage('/topic/auction/post').pipe().subscribe(x => {
      this.getAuctions();
    });

    this.sortByForm = new FormGroup({
      sort: new FormControl('price'),
      inputPriceMin: new FormControl(this.minPriceValue),
      inputPriceMax: new FormControl(this.maxPriceValue),
      inputWeightMin: new FormControl(this.minWeightValue),
      inputWeightMax: new FormControl(this.maxWeightValue),
      inputConditionMin: new FormControl(this.minConditionValue),
      inputConditionMax: new FormControl(this.maxConditionValue)});
    this.getAuctions();

    this.sortByForm.valueChanges.subscribe(value => {
      this.minPriceValue = value.inputPriceMin;
      this.maxPriceValue = value.inputPriceMax;
      this.minWeightValue = value.inputWeightMin;
      this.maxWeightValue = value.inputWeightMax;
      this.minConditionValue = value.inputConditionMin;
      this.maxConditionValue = value.inputConditionMax;
      this.getAuctions();
    });
  }

  flip() {
    this.desc = !this.desc;
    this.getAuctions();
  }

  getAuctions() {
    const cuts: string[] = new Array();
    const stones: string[] = new Array();
    const auctionRequest = new AuctionRequestParams(
      this.size,
      this.p - 1,
      this.sortByForm.value.sort,
      this.desc ? 'DESC' : 'ASC',
      null,
      null,
      this.minWeightValue,
      this.maxWeightValue,
      this.minConditionValue,
      this.maxConditionValue,
      this.minPriceValue,
      this.maxPriceValue);
    this.requestsService.getAuctions(auctionRequest)
      .subscribe((response: { count: number, auctions: Auction[] }) => {
        this.dataStorage.auctions.next(response.auctions);
        this.count = response.count;
      });
  }

}
