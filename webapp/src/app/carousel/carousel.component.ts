import {Component, ElementRef, HostListener, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style} from '@angular/animations';
import {ItemStone} from '../items/item-stone.model';
import {ItemsPopupService} from '../shared/items-popup-service';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit, OnDestroy {

  public carouselWrapperStyle = {};

  @ViewChild('carousel', {static: true}) public carousel: ElementRef;
  @Input() stopIndex: number;
  @Input() itemStones: ItemStone[];
  public speed = 5000;
  public player: AnimationPlayer;
  public itemWidth = 300;
  public currentSlide = 0;
  public timerSubscription;
  public isWidnowBig = true;


  constructor(private builder: AnimationBuilder, private itemsPopupService: ItemsPopupService) {
  }

  ngOnInit() {
    if (window.outerWidth < 1000 || window.innerWidth < 1000) {
      this.setWrapperStyle(1);
    } else {
      this.setWrapperStyle(3);
    }
  }

  setWrapperStyle(size: number) {
    this.carouselWrapperStyle = {
      width: `${this.itemWidth * size}px`
    };
  }

  ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.outerWidth < 1000 || event.target.innerWidth < 1000) {
      this.isWidnowBig = false;
      this.setWrapperStyle(1);
    } else {
      this.isWidnowBig = true;
      this.setWrapperStyle(3);
    }
  }

  startSpinning() {
    let difference = 0;
    if (window.outerWidth < 1000 || window.innerWidth < 1000) {
      difference++;
    }
    const random = Math.random() - 0.5;
    const offset = ((this.stopIndex + difference) * this.itemWidth) + (random * this.itemWidth / 4);
    const myAnimation: AnimationFactory = this.buildAnimation(-offset);
    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
    this.player.onDone(() => {
      setTimeout(timeout => {
        const itemStone: ItemStone[] = new Array();
        itemStone.push(this.itemStones[this.stopIndex + 1]);
        this.itemsPopupService.openPopup({itemStones: itemStone, size: 300});
      }, 1000);
    });
  }

  resetCarousel() {
    const myAnimation: AnimationFactory = this.buildAnimation(0);
    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
    this.currentSlide = 0;
  }

  private buildAnimation(offset) {
    return this.builder.build([
      animate(this.speed + 'ms cubic-bezier(.12,.68,.2, 1)', style({transform: `translateX(${offset}px)`}))
    ]);
  }
}

