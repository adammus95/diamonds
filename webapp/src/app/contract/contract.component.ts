import {Component, OnInit, ViewChild} from '@angular/core';
import {ItemStone} from '../items/item-stone.model';
import {FormControl, FormGroup} from '@angular/forms';
import {RequestsService} from '../shared/requests.service';
import {Router} from '@angular/router';
import {SocketClientService} from '../websocket/socket-client.service';
import {ItemStoneComponent} from '../items/item-stone/item-stone.component';
import {timer} from 'rxjs';

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.css']
})
export class ContractComponent implements OnInit {

  @ViewChild('userItems', {static: true}) userItems;
  @ViewChild('winItemStone', {static: true}) winItemStone: ItemStoneComponent;

  public size = 5;
  public auctionEmptySpaces: number[];
  public itemStonesRemovedFromEq: ItemStone[] = new Array();
  public canvasSize = 100;

  public itemStone: ItemStone;
  public timerSubscription;

  public isEmpty = true;

  public newAuctionForm: FormGroup;

  constructor(private requestsService: RequestsService, private router: Router, private socketClientService: SocketClientService) {
  }

  ngOnInit() {
    if (window.innerWidth < 1000 || window.outerWidth < 1000) {
      this.canvasSize = 50;
    }
    this.newAuctionForm = new FormGroup({price: new FormControl(), isBiddable: new FormControl()});
    this.auctionEmptySpaces = Array(this.size).fill(0).map((x, i) => i);
  }

  onEqItemRemoved(itemStone: ItemStone) {
    if (itemStone && this.itemStonesRemovedFromEq.length < this.size) {
      this.itemStonesRemovedFromEq.push(itemStone);
      this.auctionEmptySpaces.pop();
    }
  }

  onAuctionItemRemoved(itemStone: ItemStone) {
    if (itemStone) {
      this.userItems.restoreItem(itemStone);
    }
  }

  onSignContract() {
    this.requestsService.signContract(this.itemStonesRemovedFromEq.map(item => item.id)).subscribe((response: ItemStone) => {

      let winIndex = 0;
      this.itemStonesRemovedFromEq.forEach((item: ItemStone) => {
        if (item.id === response.id) {
          winIndex = this.itemStonesRemovedFromEq.indexOf(item);
        }
      });

      let selectedIndex = 0;
      let i = 0;

      const source = timer(0, 50);
      this.timerSubscription = source.subscribe(val => {
        if (selectedIndex > 0) {
          this.itemStonesRemovedFromEq[selectedIndex - 1].selected = false;
        }
        if (selectedIndex === this.size) {
          this.itemStonesRemovedFromEq[this.size - 1].selected = false;
          selectedIndex = 0;
        }
        this.itemStonesRemovedFromEq[selectedIndex].selected = true;
        selectedIndex++;
        i++;
        if (i > this.size * 3 && this.itemStonesRemovedFromEq[winIndex].id === this.itemStonesRemovedFromEq[selectedIndex - 1].id) {
          setTimeout(t => {
            this.itemStone = response;
            this.isEmpty = false;
            this.winItemStone.itemStone = this.itemStone;
            this.itemStonesRemovedFromEq = [];
            for (let x = 0; x < this.size; x++) {
              this.auctionEmptySpaces.push(0);
            }
            this.winItemStone.drawItemStone();
          }, 2000);
          this.timerSubscription.unsubscribe();
        }
      });
    });
  }

}


