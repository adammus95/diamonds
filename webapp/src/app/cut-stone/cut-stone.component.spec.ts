import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CutStoneComponent } from './cut-stone.component';

describe('CutStoneComponent', () => {
  let component: CutStoneComponent;
  let fixture: ComponentFixture<CutStoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CutStoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CutStoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
