import {Component, OnInit, ViewChild} from '@angular/core';
import {ItemStone} from '../items/item-stone.model';
import {FormControl, FormGroup} from '@angular/forms';
import {ItemsComponent} from '../items/items.component';
import {RequestsService} from '../shared/requests.service';

@Component({
  selector: 'app-cut-stone',
  templateUrl: './cut-stone.component.html',
  styleUrls: ['./cut-stone.component.css']
})
export class CutStoneComponent implements OnInit {

  @ViewChild('userItems', {static: true}) userItems;
  @ViewChild('itemToCut', {static: false}) itemToCut: ItemsComponent;
  public cutEmptySpaces: number[];
  public previewEmptySpaces: number[];
  public itemStonesRemovedFromEq: ItemStone[] = new Array();
  public itemStonesPreview: {itemStone: ItemStone, selected: boolean}[] = new Array();
  public isItemStonesPreviewEmpty = true;
  public selectedCut: string;
  public size = 1;
  public orginalWeight = 0;

  public cutForm: FormGroup;

  constructor(private requestsService: RequestsService) {
  }

  ngOnInit() {
    this.cutForm = new FormGroup({slider: new FormControl(100)});
    this.cutEmptySpaces = Array(this.size).fill(0).map((x, i) => i);
    this.previewEmptySpaces = Array(3).fill(0).map((x, i) => i);

    this.cutForm.valueChanges.subscribe(value => {
      this.itemStonesRemovedFromEq[0].weight = Number((this.orginalWeight * value.slider / 100).toFixed(2));
      this.itemStonesRemovedFromEq[0].weight = this.itemStonesRemovedFromEq[0].weight < 0.01 ? 0.01 :  this.itemStonesRemovedFromEq[0].weight;
      this.itemToCut.redrawItems();
    });
  }

  setCut(itemStone: {itemStone: ItemStone, selected: boolean}) {
    this.itemStonesPreview.forEach( x => x.selected = false);
    this.selectedCut = itemStone.itemStone.cut;
    itemStone.selected = true;
  }

  onEqItemRemoved(itemStone: ItemStone) {
    if (this.itemStonesRemovedFromEq.length < this.size) {
      this.orginalWeight = itemStone.weight;
      this.itemStonesRemovedFromEq.push(itemStone);
      this.isItemStonesPreviewEmpty = false;
      const tempItemStone1 = JSON.parse(JSON.stringify(itemStone));
      tempItemStone1.cut = 'OVAL';
      this.itemStonesPreview.push({itemStone: tempItemStone1, selected: false});
      const tempItemStone2 = JSON.parse(JSON.stringify(itemStone));
      tempItemStone2.cut = 'BRILLIANT';
      this.itemStonesPreview.push({itemStone: tempItemStone2, selected: false});
      const tempItemStone3 = JSON.parse(JSON.stringify(itemStone));
      tempItemStone3.cut = 'OCTAGON';
      this.itemStonesPreview.push({itemStone: tempItemStone3, selected: false});

      this.cutEmptySpaces.pop();
    }
  }

  onCut() {
    if (this.selectedCut) {
      this.requestsService.cut(
        this.itemStonesRemovedFromEq[0].id,
        this.selectedCut,
        this.cutForm.value.slider).subscribe( (itemStone: ItemStone) => {
        if (itemStone.removed) {
          this.itemStonesRemovedFromEq.pop();
          this.userItems.restoreItem(null);
          this.cutEmptySpaces.push(1);
          this.isItemStonesPreviewEmpty = true;
          this.itemStonesPreview = [];
        } else {
          this.itemStonesRemovedFromEq[0] = itemStone;
        }
      });
    }
  }

  onAuctionItemRemoved(itemStone: ItemStone) {
    this.isItemStonesPreviewEmpty = true;
    this.userItems.restoreItem(itemStone);
    this.itemStonesPreview = [];
  }

}
