import {AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {RequestsService} from '../shared/requests.service';
import {Draw} from '../user-details/draw.model';
import {SocketClientService} from '../websocket/socket-client.service';
import {DataStorageService} from '../shared/data-storage.service';
import {animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style} from '@angular/animations';

@Component({
  selector: 'app-draws',
  templateUrl: './draws.component.html',
  styleUrls: ['./draws.component.css']
})
export class DrawsComponent implements OnInit, AfterViewInit {

  @ViewChild('carousel', {static: true}) public carousel: ElementRef;
  @ViewChildren('items') public itemStones: QueryList<ElementRef>;
  public itemStonesNativeElementsQuery: QueryList<ElementRef>;
  public displayDraws: Draw[] = new Array();
  public querryLenght = 0;
  public summaryQuerryLength = 0;
  public itemWidth = 111;
  public player: AnimationPlayer;

  constructor(private requestsService: RequestsService,
              private socketClientService: SocketClientService,
              private dataStorage: DataStorageService,
              private builder: AnimationBuilder) {
  }

  ngOnInit() {
    this.getDraws();
    this.dataStorage.draws.subscribe((draws: Draw[]) => {

      if (this.summaryQuerryLength > 0) {
        this.moveCarousel(this.summaryQuerryLength);
      }
      if (this.player && this.player.hasStarted()) {
        this.player.onDone(() => {
          this.querryLenght--;
          if (this.querryLenght === 0) {
            this.displayDraws = draws;
            this.summaryQuerryLength = 0;
          }
        });
      } else {
        if (this.querryLenght === 0) {
          this.displayDraws = draws;
          this.summaryQuerryLength = 0;
        }
      }
    });

    this.socketClientService.onMessage('/topic/draw/post').pipe().subscribe(x => {
      this.querryLenght++;
      this.summaryQuerryLength++;
      this.getDraws();
    });
  }

  moveCarousel(length: number) {
    this.itemStonesNativeElementsQuery.forEach(item => {
      const offset = this.itemWidth * length;
      const myAnimation: AnimationFactory = this.buildAnimation(offset, 500);
      this.player = myAnimation.create(item.nativeElement);
      this.player.play();
    });
    this.player.onDone(() => {
    });
  }

  ngAfterViewInit(): void {
    this.itemStones.changes.subscribe((items: QueryList<ElementRef>) => {
      this.itemStonesNativeElementsQuery = items;
    });
  }

  private buildAnimation(offset, speed) {
    return this.builder.build([
      animate(speed + 'ms ease', style({transform: `translateX(${offset}px)`}))
    ]);
  }

  getDraws() {
    this.requestsService.getLastDraws().subscribe((response: Draw[]) => {
      this.dataStorage.draws.next(response);
    });
  }

}
