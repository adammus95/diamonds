import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoginService} from '../login/login.service';
import {Subscription} from 'rxjs';
import {DataStorageService} from '../shared/data-storage.service';
import {Router} from '@angular/router';
import {User} from '../login/user.model';
import {FormControl, FormGroup} from '@angular/forms';
import {RequestsService} from '../shared/requests.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public isLoggedIn = false;
  public userSub: Subscription;
  public user: User;
  public users: Array<{id: number, username: string}> = new Array();
  public isInputBlured = true;

  public search: FormGroup;

  constructor(private loginService: LoginService, private dataStorage: DataStorageService, private router: Router, private requestsService: RequestsService) {
  }

  ngOnInit() {

    this.search = new FormGroup({
      search: new FormControl('')
    });

    this.search.valueChanges.subscribe(value => {
      this.users = [];
      if (value.search.length > 2) {
        this.requestsService.searchUser(value.search).subscribe((response: Array<{id: number, username: string}>) => {
          this.users = response;
        });
      }
    });

    this.userSub = this.dataStorage.user.subscribe(user => {
      this.isLoggedIn = !!user;
      if (!!user) {
        this.user = user;
      }
    });
  }

  onBlur(event) {
    this.isInputBlured = true;
  }

  onFocus(event) {
    this.isInputBlured = false;
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

  logOut() {
    this.loginService.logout();
  }

}
