import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RequestsService} from '../shared/requests.service';
import {ItemStone} from '../items/item-stone.model';
import {CarouselComponent} from '../carousel/carousel.component';
import {animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style} from '@angular/animations';
import {DataStorageService} from '../shared/data-storage.service';
import {Draw} from '../user-details/draw.model';
import {Time, TimeService} from '../shared/time.service';
import {timer} from 'rxjs';
import {SocketClientService} from '../websocket/socket-client.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  public itemStones: ItemStone[] = new Array();
  public disabled = true;
  public stopIndex: number;
  public player: AnimationPlayer;
  public openSpeed = 1000;
  public cooldown = 1000 * 20;
  public date;
  public timerSubscription;
  public timeLeft;
  public endTime = new Time('', 0, 0, 0, 0);

  @ViewChild(CarouselComponent, {static: true}) carousel: CarouselComponent;
  @ViewChild('doorLeft', {static: true}) private doorLeft: ElementRef;
  @ViewChild('doorRight', {static: true}) private doorRight: ElementRef;

  constructor(private timeService: TimeService,
              private requestsService: RequestsService,
              private dataStorage: DataStorageService,
              private builder: AnimationBuilder,
              private socketClientService: SocketClientService) {
  }

  ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.stopIndex = Math.floor(Math.random() * 20) + 10;
    this.dataStorage.getUserData().subscribe(user => {
      if (user) {
        this.drawGhostItems();
        this.getDrawTime();
      }
    });

  }

  restart() {
    this.drawGhostItems();
    this.getDrawTime();
    this.itemStones = new Array();
    this.closeDoors();
    this.carousel.resetCarousel();
  }

  getDrawTime() {
    this.requestsService.getLastUserDraw()
      .subscribe((response: Draw) => {
        if (response) {
          this.date = this.timeService.timestampToLocalTime(response.drawTime);
          this.startTimer();
        } else {
          this.disabled = false;
        }
      });
  }

  startTimer() {
    const source = timer(0, 1000);
    this.timerSubscription = source.subscribe(val => {
      this.timeLeft = Number(this.date.getTime() + this.cooldown - new Date().getTime() - this.dataStorage.dateTimeOffset);
      if (this.timeLeft <= 0) {
        this.disabled = false;
        this.timerSubscription.unsubscribe();
      }
      this.endTime = this.timeService.getTime(this.timeLeft / 1000);
    });
  }

  openDoors() {
    const leftDoorAnimation: AnimationFactory = this.openDoorAnimation(-300);
    const rightDoorAnimation: AnimationFactory = this.openDoorAnimation(300);
    this.player = leftDoorAnimation.create(this.doorLeft.nativeElement);
    this.player.play();
    this.player = rightDoorAnimation.create(this.doorRight.nativeElement);
    this.player.play();
  }

  closeDoors() {
    const leftDoorAnimation: AnimationFactory = this.closeDoorAnimation(0);
    const rightDoorAnimation: AnimationFactory = this.closeDoorAnimation(0);
    this.player = leftDoorAnimation.create(this.doorLeft.nativeElement);
    this.player.play();
    this.player = rightDoorAnimation.create(this.doorRight.nativeElement);
    this.player.play();
  }

  private openDoorAnimation(offset) {
    return this.builder.build([
      animate(this.openSpeed + 'ms ease', style({width: '20%'}))
    ]);
  }

  private closeDoorAnimation(offset) {
    return this.builder.build([
      animate(this.openSpeed + 'ms ease', style({width: '50%'}))
    ]);
  }

  drawGhostItems() {
    this.requestsService.drawGhostStones(this.stopIndex + 1).subscribe((itemStones: ItemStone[]) => {
      this.itemStones = itemStones;
    });
  }

  drawItem() {
    this.disabled = true;
    this.requestsService.drawStone()
      .subscribe((itemStone: ItemStone) => {
        this.openDoors();
        setTimeout(e => {
          this.itemStones.splice(this.stopIndex, 0, itemStone);
          this.carousel.startSpinning();
        }, this.openSpeed);
        setTimeout(e => {
          this.restart();
        }, 8000);
      });
  }

}
