import {Component, Inject, Input, OnInit} from '@angular/core';
import {ItemStone} from '../items/item-stone.model';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-items-popup',
  templateUrl: './items-popup.component.html',
  styleUrls: ['./items-popup.component.css']
})
export class ItemsPopupComponent implements OnInit {

  public items: ItemStone[];
  public size = 150;

  constructor(@Inject(MAT_DIALOG_DATA) data: { itemStones: ItemStone[], size: number }) {
    this.items = data.itemStones;
  }

  ngOnInit() {
  }

}
