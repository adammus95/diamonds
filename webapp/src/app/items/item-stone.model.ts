import {Cut} from './cut.enum';

export class ItemStone {
  constructor(public available: boolean, private _id: number, public stone: string, public cut: string, public condition: number, public weight: number, public color: string, public removed: boolean, public selected: boolean) {}

  get id() {
    return this._id;
  }
}
