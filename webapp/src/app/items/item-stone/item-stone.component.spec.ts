import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemStoneComponent } from './item-stone.component';

describe('ItemStoneComponent', () => {
  let component: ItemStoneComponent;
  let fixture: ComponentFixture<ItemStoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemStoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemStoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
