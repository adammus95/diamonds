import {AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ItemStone} from '../item-stone.model';
import {DataStorageService} from '../../shared/data-storage.service';

@Component({
  selector: 'app-item-stone',
  templateUrl: './item-stone.component.html',
  styleUrls: ['./item-stone.component.css']
})
export class ItemStoneComponent implements OnInit, AfterViewInit, AfterViewChecked {

  @ViewChild('canvas', {static: true}) canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;

  public canvasXCenter = 0;
  public canvasYCenter = 0;
  public rgb;
  public lineWidth = 3;
  public stoneDrawSize;
  public conditionImage;

  @Input() isEmpty: boolean;
  @Input() itemStone: ItemStone;
  @Input() size: number;
  @Input() index: number;
  @Input() canvasSize = 150;
  @Input() itemInfo = true;
  @Input() displayItemInfo = this.itemInfo;

  constructor(private dataStorage: DataStorageService, private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.conditionImage = this.dataStorage.conditionImage;
  }

  ngAfterViewInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.ctx.canvas.width = this.canvasSize;
    this.ctx.canvas.height = this.canvasSize;

    if (!this.isEmpty) {
      this.drawItemStone();
    }
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  drawItemStone() {

    this.ctx.clearRect(0, 0, this.canvasSize, this.canvasSize);

    this.stoneDrawSize = this.canvasSize * Math.pow(this.itemStone.weight, 0.2);
    this.lineWidth = this.lineWidth * Math.pow(this.itemStone.weight, 0.2);

    const itemStone = this.itemStone;
    const r = itemStone.color.substring(0, 3);
    const g = itemStone.color.substring(3, 6);
    const b = itemStone.color.substring(6, 9);
    this.rgb = 'rgb(' + r + ',' + g + ',' + b + ')';

    this.canvasXCenter = this.ctx.canvas.width / 2;
    this.canvasYCenter = this.ctx.canvas.height / 2;
    this.ctx.lineWidth = this.lineWidth;

    this.ctx.strokeStyle = 'rgb(40,40,40)';

    switch (this.itemStone.cut) {
      case null: {
        this.noCut();
        break;
      }
      case 'OCTAGON': {
        this.octagonCut();
        break;
      }
      case 'OVAL': {
        this.ovalCut();
        break;
      }
      case 'BRILLIANT': {
        this.brilliantCut();
        break;
      }
      case 'NO_CUT_1': {
        this.noCut();
        break;
      }
      case 'NO_CUT_2': {
        this.noCut2();
        break;
      }
      case 'NO_CUT_3': {
        this.noCut3();
        break;
      }
      case 'NO_CUT_4': {
        this.noCut4();
        break;
      }
      case 'NO_CUT_5': {
        this.noCut5();
        break;
      }
    }

    this.drawItemCondition();

  }

  noCut() {

    const width = this.stoneDrawSize;
    const height = this.stoneDrawSize;
    const canvasXCenter = this.canvasXCenter;
    const canvasYCenter = this.canvasYCenter;

    this.ctx.beginPath();
    this.ctx.moveTo(canvasXCenter - width * 0.35, canvasYCenter);
    this.ctx.lineTo(canvasXCenter - width * 0.25, canvasYCenter - height * 0.25);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter - height * 0.28);
    this.ctx.lineTo(canvasXCenter + width * 0.25, canvasYCenter - height * 0.25);
    this.ctx.lineTo(canvasXCenter + width * 0.35, canvasYCenter - height * 0);
    this.ctx.lineTo(canvasXCenter + width * 0.25, canvasYCenter + height * 0.25);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter + height * 0.3);
    this.ctx.lineTo(canvasXCenter - width * 0.2, canvasYCenter + height * 0.3);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter + height * 0.25);
    this.ctx.closePath();

    const grd = this.ctx.createLinearGradient(0, 0, canvasXCenter + width * 0.1, 0);
    grd.addColorStop(0, 'white');
    grd.addColorStop(1, this.rgb);
    this.ctx.fillStyle = grd;
    this.ctx.fill();

    this.ctx.stroke();

  }

  noCut2() {

    const width = this.stoneDrawSize;
    const height = this.stoneDrawSize;
    const canvasXCenter = this.canvasXCenter;
    const canvasYCenter = this.canvasYCenter;

    this.ctx.beginPath();
    this.ctx.moveTo(canvasXCenter - width * 0.3, canvasYCenter);
    this.ctx.lineTo(canvasXCenter - width * 0.2, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter - height * 0.25);
    this.ctx.lineTo(canvasXCenter + width * 0.2, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter + width * 0.25, canvasYCenter + height * 0.3);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter + height * 0.4);
    this.ctx.lineTo(canvasXCenter - width * 0.2, canvasYCenter + height * 0.4);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter + height * 0.3);
    this.ctx.closePath();

    const grd = this.ctx.createLinearGradient(0, 0, canvasXCenter + width * 0.1, 0);
    grd.addColorStop(0, 'white');
    grd.addColorStop(1, this.rgb);
    this.ctx.fillStyle = grd;
    this.ctx.fill();

    this.ctx.stroke();


  }

  noCut3() {

    const width = this.stoneDrawSize;
    const height = this.stoneDrawSize;
    const canvasXCenter = this.canvasXCenter;
    const canvasYCenter = this.canvasYCenter;

    this.ctx.beginPath();
    this.ctx.moveTo(canvasXCenter - width * 0.3, canvasYCenter);
    this.ctx.lineTo(canvasXCenter - width * 0.2, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter, canvasYCenter - height * 0.2);
    this.ctx.lineTo(canvasXCenter + width * 0.35, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter + width * 0.4, canvasYCenter + height * 0.3);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter + height * 0.35);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter + height * 0.4);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter + height * 0.3);
    this.ctx.closePath();

    const grd = this.ctx.createLinearGradient(0, 0, canvasXCenter + width * 0.1, 0);
    grd.addColorStop(0, 'white');
    grd.addColorStop(1, this.rgb);
    this.ctx.fillStyle = grd;
    this.ctx.fill();

    this.ctx.stroke();

  }

  noCut4() {

    const width = this.stoneDrawSize;
    const height = this.stoneDrawSize;
    const canvasXCenter = this.canvasXCenter;
    const canvasYCenter = this.canvasYCenter;

    this.ctx.beginPath();
    this.ctx.moveTo(canvasXCenter - width * 0.4, canvasYCenter);
    this.ctx.lineTo(canvasXCenter - width * 0.2, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter - height * 0.25);
    this.ctx.lineTo(canvasXCenter + width * 0.3, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter + width * 0.35, canvasYCenter + height * 0.3);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter + height * 0.4);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter + height * 0.4);
    this.ctx.lineTo(canvasXCenter - width * 0.2, canvasYCenter + height * 0.3);
    this.ctx.closePath();

    const grd = this.ctx.createLinearGradient(0, 0, canvasXCenter + width * 0.1, 0);
    grd.addColorStop(0, 'white');
    grd.addColorStop(1, this.rgb);
    this.ctx.fillStyle = grd;
    this.ctx.fill();

    this.ctx.stroke();


  }

  noCut5() {

    const width = this.stoneDrawSize;
    const height = this.stoneDrawSize;
    const canvasXCenter = this.canvasXCenter;
    const canvasYCenter = this.canvasYCenter;

    this.ctx.beginPath();
    this.ctx.moveTo(canvasXCenter - width * 0.45, canvasYCenter);
    this.ctx.lineTo(canvasXCenter - width * 0.2, canvasYCenter - height * 0.25);
    this.ctx.lineTo(canvasXCenter - width * 0.05, canvasYCenter - height * 0.2);
    this.ctx.lineTo(canvasXCenter + width * 0.25, canvasYCenter - height * 0.1);
    this.ctx.lineTo(canvasXCenter + width * 0.35, canvasYCenter + height * 0.3);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter + height * 0.4);
    this.ctx.lineTo(canvasXCenter - width * 0.25, canvasYCenter + height * 0.4);
    this.ctx.lineTo(canvasXCenter - width * 0.35, canvasYCenter + height * 0.3);
    this.ctx.closePath();

    const grd = this.ctx.createLinearGradient(0, 0, canvasXCenter + width * 0.1, 0);
    grd.addColorStop(0, 'white');
    grd.addColorStop(1, this.rgb);
    this.ctx.fillStyle = grd;
    this.ctx.fill();

    this.ctx.stroke();

  }

  ovalCut() {

    const width = this.stoneDrawSize;
    const height = this.stoneDrawSize;
    const canvasXCenter = this.canvasXCenter;
    const canvasYCenter = this.canvasYCenter;

    this.ctx.beginPath();
    this.ctx.ellipse(canvasXCenter, canvasYCenter, width * 0.4, height * 0.27, Math.PI, 0, Math.PI * 2);
    const grd = this.ctx.createLinearGradient(0, 0, canvasXCenter - width * 0.1, 0);
    grd.addColorStop(0, 'white');
    grd.addColorStop(1, this.rgb);
    this.ctx.fillStyle = grd;
    this.ctx.fillStyle = grd;
    this.ctx.fill();

    this.ctx.moveTo(canvasXCenter - width * 0.28, canvasYCenter - height * 0.06);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter - height * 0.18);
    this.ctx.lineTo(canvasXCenter + width * 0.28, canvasYCenter + height * 0.06);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter + height * 0.18);
    this.ctx.closePath();
    this.ctx.moveTo(canvasXCenter - width * 0.1, canvasYCenter - height * 0.18);
    this.ctx.lineTo(canvasXCenter + width * 0.28, canvasYCenter - height * 0.06);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter + height * 0.18);
    this.ctx.lineTo(canvasXCenter - width * 0.28, canvasYCenter + height * 0.06);
    this.ctx.closePath();
    this.ctx.moveTo(canvasXCenter - width * 0.28, canvasYCenter - height * 0.06);
    this.ctx.lineTo(canvasXCenter - width * 0.28, canvasYCenter - height * 0.18);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter - height * 0.18);
    this.ctx.lineTo(canvasXCenter, canvasYCenter - height * 0.28);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter - height * 0.18);
    this.ctx.lineTo(canvasXCenter + width * 0.28, canvasYCenter - height * 0.18);
    this.ctx.lineTo(canvasXCenter + width * 0.28, canvasYCenter - height * 0.06);
    this.ctx.lineTo(canvasXCenter + width * 0.39, canvasYCenter);
    this.ctx.lineTo(canvasXCenter + width * 0.28, canvasYCenter + height * 0.06);
    this.ctx.lineTo(canvasXCenter + width * 0.28, canvasYCenter + height * 0.18);
    this.ctx.lineTo(canvasXCenter + width * 0.1, canvasYCenter + height * 0.18);
    this.ctx.lineTo(canvasXCenter, canvasYCenter + height * 0.28);
    this.ctx.lineTo(canvasXCenter - width * 0.1, canvasYCenter + height * 0.18);
    this.ctx.lineTo(canvasXCenter - width * 0.28, canvasYCenter + height * 0.18);
    this.ctx.lineTo(canvasXCenter - width * 0.28, canvasYCenter + height * 0.06);
    this.ctx.lineTo(canvasXCenter - width * 0.39, canvasYCenter);
    this.ctx.closePath();

    this.ctx.moveTo(canvasXCenter - width * 0.28, canvasYCenter - height * 0.06);
    this.ctx.lineTo(canvasXCenter - width * 0.36, canvasYCenter - height * 0.1);
    this.ctx.moveTo(canvasXCenter + width * 0.1, canvasYCenter - height * 0.18);
    this.ctx.lineTo(canvasXCenter + width * 0.16, canvasYCenter - height * 0.24);
    this.ctx.moveTo(canvasXCenter + width * 0.28, canvasYCenter + height * 0.06);
    this.ctx.lineTo(canvasXCenter + width * 0.36, canvasYCenter + height * 0.1);
    this.ctx.moveTo(canvasXCenter - width * 0.1, canvasYCenter + height * 0.18);
    this.ctx.lineTo(canvasXCenter - width * 0.16, canvasYCenter + height * 0.24);

    this.ctx.moveTo(canvasXCenter - width * 0.1, canvasYCenter - height * 0.18);
    this.ctx.lineTo(canvasXCenter - width * 0.16, canvasYCenter - height * 0.24);
    this.ctx.moveTo(canvasXCenter + width * 0.28, canvasYCenter - height * 0.06);
    this.ctx.lineTo(canvasXCenter + width * 0.36, canvasYCenter - height * 0.1);
    this.ctx.moveTo(canvasXCenter + width * 0.1, canvasYCenter + height * 0.18);
    this.ctx.lineTo(canvasXCenter + width * 0.16, canvasYCenter + height * 0.24);
    this.ctx.moveTo(canvasXCenter - width * 0.28, canvasYCenter + height * 0.06);
    this.ctx.lineTo(canvasXCenter - width * 0.36, canvasYCenter + height * 0.1);

    this.ctx.fill();

    this.ctx.stroke();

  }

  octagonCut() {

    const width = this.stoneDrawSize;
    const height = this.stoneDrawSize;
    const canvasXCenter = this.canvasXCenter;
    const canvasYCenter = this.canvasYCenter;

    this.ctx.beginPath();
    this.ctx.moveTo(canvasXCenter - width * 0.4, canvasYCenter + height * 0.15);
    this.ctx.lineTo(canvasXCenter - width * 0.4, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter - height * 0.25);
    this.ctx.lineTo(canvasXCenter + width * 0.3, canvasYCenter - height * 0.25);
    this.ctx.lineTo(canvasXCenter + width * 0.4, canvasYCenter - height * 0.15);
    this.ctx.lineTo(canvasXCenter + width * 0.4, canvasYCenter + height * 0.15);
    this.ctx.lineTo(canvasXCenter + width * 0.3, canvasYCenter + height * 0.25);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter + height * 0.25);

    this.ctx.moveTo(canvasXCenter - width * 0.275, canvasYCenter + height * 0.075);
    this.ctx.lineTo(canvasXCenter - width * 0.275, canvasYCenter - height * 0.075);
    this.ctx.lineTo(canvasXCenter - width * 0.225, canvasYCenter - height * 0.125);
    this.ctx.lineTo(canvasXCenter + width * 0.225, canvasYCenter - height * 0.125);
    this.ctx.lineTo(canvasXCenter + width * 0.275, canvasYCenter - height * 0.075);
    this.ctx.lineTo(canvasXCenter + width * 0.275, canvasYCenter + height * 0.075);
    this.ctx.lineTo(canvasXCenter + width * 0.225, canvasYCenter + height * 0.125);
    this.ctx.lineTo(canvasXCenter - width * 0.225, canvasYCenter + height * 0.125);

    this.ctx.closePath();
    const grd = this.ctx.createLinearGradient(0, 0, canvasXCenter + width * 0.1, 0);
    grd.addColorStop(0, 'white');
    grd.addColorStop(1, this.rgb);
    this.ctx.fillStyle = grd;
    this.ctx.fill();

    this.ctx.moveTo(canvasXCenter - width * 0.275, canvasYCenter + height * 0.075);
    this.ctx.lineTo(canvasXCenter - width * 0.4, canvasYCenter + height * 0.15);
    this.ctx.moveTo(canvasXCenter - width * 0.275, canvasYCenter - height * 0.075);
    this.ctx.lineTo(canvasXCenter - width * 0.4, canvasYCenter - height * 0.15);
    this.ctx.moveTo(canvasXCenter - width * 0.225, canvasYCenter - height * 0.125);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter - height * 0.25);
    this.ctx.moveTo(canvasXCenter + width * 0.225, canvasYCenter - height * 0.125);
    this.ctx.lineTo(canvasXCenter + width * 0.3, canvasYCenter - height * 0.25);
    this.ctx.moveTo(canvasXCenter + width * 0.275, canvasYCenter - height * 0.075);
    this.ctx.lineTo(canvasXCenter + width * 0.4, canvasYCenter - height * 0.15);
    this.ctx.moveTo(canvasXCenter + width * 0.275, canvasYCenter + height * 0.075);
    this.ctx.lineTo(canvasXCenter + width * 0.4, canvasYCenter + height * 0.15);
    this.ctx.moveTo(canvasXCenter + width * 0.225, canvasYCenter + height * 0.125);
    this.ctx.lineTo(canvasXCenter + width * 0.3, canvasYCenter + height * 0.25);
    this.ctx.moveTo(canvasXCenter - width * 0.225, canvasYCenter + height * 0.125);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter + height * 0.25);

    this.ctx.stroke();

  }

  brilliantCut() {

    const width = this.stoneDrawSize;
    const height = this.stoneDrawSize;
    const canvasXCenter = this.canvasXCenter;
    const canvasYCenter = this.canvasYCenter;

    this.ctx.beginPath();

    this.ctx.moveTo(canvasXCenter, canvasYCenter + height * 0.6);
    this.ctx.lineTo(canvasXCenter - width * 0.5, canvasYCenter - height * 0.05);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter - height * 0.3);
    this.ctx.lineTo(canvasXCenter + width * 0.3, canvasYCenter - height * 0.3);
    this.ctx.lineTo(canvasXCenter + width * 0.5, canvasYCenter - height * 0.05);
    this.ctx.lineTo(canvasXCenter, canvasYCenter + height * 0.6);
    this.ctx.closePath();
    this.ctx.moveTo(canvasXCenter, canvasYCenter + height * 0.6);
    this.ctx.lineTo(canvasXCenter - width * 0.3, canvasYCenter - height * 0.3);
    this.ctx.moveTo(canvasXCenter, canvasYCenter + height * 0.6);
    this.ctx.lineTo(canvasXCenter + width * 0.3, canvasYCenter - height * 0.3);
    this.ctx.moveTo(canvasXCenter - width * 0.5, canvasYCenter - height * 0.05);
    this.ctx.lineTo(canvasXCenter + width * 0.5, canvasYCenter - height * 0.05);
    this.ctx.moveTo(canvasXCenter, canvasYCenter - height * 0.3);
    this.ctx.lineTo(canvasXCenter - width * 0.20, canvasYCenter - height * 0.05);
    this.ctx.moveTo(canvasXCenter, canvasYCenter - height * 0.3);
    this.ctx.lineTo(canvasXCenter + width * 0.20, canvasYCenter - height * 0.05);

    const grd = this.ctx.createLinearGradient(0, 0, canvasXCenter + height * 0.2, 0);
    grd.addColorStop(0, 'white');

    grd.addColorStop(1, this.rgb);
    this.ctx.fillStyle = grd;
    this.ctx.fill();

    this.ctx.stroke();

  }

  drawItemCondition() {
      this.ctx.globalAlpha = (100 - this.itemStone.condition) / 100;
      this.ctx.fillStyle = this.ctx.createPattern(this.conditionImage, 'no-repeat');
      this.ctx.fill();
      this.ctx.globalAlpha = 1;
  }

}
