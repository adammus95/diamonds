import {Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {RequestsService} from '../shared/requests.service';
import {ActivatedRoute} from '@angular/router';
import {ItemStone} from './item-stone.model';
import {FormControl, FormGroup} from '@angular/forms';
import {ItemStoneComponent} from './item-stone/item-stone.component';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  @Output() itemRemoved = new EventEmitter<ItemStone>();
  @ViewChildren('items') private items: QueryList<ItemStoneComponent>;

  @Input() isUserEq: boolean;
  @Input() isClickable = true;
  @Input() disableSorting: boolean;
  @Input() disablePaging: boolean;
  @Input() emptySpaces: number[];
  @Input() itemStones: ItemStone[];
  @Input() canvasSize = 150;
  @Input() public size = 15;
  @Input() public maxItemsToRemove = 15;
  @Input() public drawItemInfo = true;

  public sortByForm: FormGroup;
  public desc = true;

  public itemStonesRemoved: ItemStone[] = new Array();

  public count: number;
  public p = 1;


  constructor(private requestsService: RequestsService, private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.sortByForm = new FormGroup({sort: new FormControl('Weight')});

    if (this.isUserEq) {
      this.userEq();
    }
  }

  redrawItems() {
    this.items.forEach((item: ItemStoneComponent) => {
      item.drawItemStone();
    });
  }

  onItemRemove(itemStone: ItemStone) {
    if (itemStone.available && this.isClickable) {
      if (this.isUserEq) {
        if (this.itemStonesRemoved.length < this.maxItemsToRemove) {
          this.itemStones.splice(this.itemStones.indexOf(itemStone), 1);
          this.itemRemoved.emit(itemStone);
          this.emptySpaces.push(1);
          this.itemStonesRemoved.push(itemStone);
        }
      } else {
        this.itemStones.splice(this.itemStones.indexOf(itemStone), 1);
        this.itemRemoved.emit(itemStone);
        this.emptySpaces.push(1);
        this.itemStonesRemoved.push(itemStone);
      }
    } else {
      this.itemRemoved.emit(null);
    }
  }

  public restoreItem(itemStone: ItemStone) {
    this.itemStonesRemoved.splice(this.itemStonesRemoved.indexOf(itemStone), 1);
    this.getItems();
  }

  userEq() {
    this.getItems();
    this.sortByForm.valueChanges.subscribe(value => {
      this.getItems();
    });
  }

  flip() {
    this.desc = !this.desc;
    this.getItems();
  }

  fillEmptySpaces() {
    this.emptySpaces = Array(this.size - this.itemStones.length).fill(0).map((x, i) => i);
  }

  getItems() {

    const username = (this.isUserEq) ? JSON.parse(localStorage.getItem('userData')).username : this.route.snapshot.params[`id`];
    this.requestsService.getUserItemStones(username, this.size, this.p - 1, this.sortByForm.value[`sort`], this.desc ? 'DESC' : 'ASC')
      .subscribe((response: { count: number, itemStones: ItemStone[] }) => {
        this.itemStones = response.itemStones;
        this.count = response.count;
        this.fillEmptySpaces();
        this.itemStonesRemoved.forEach(i => {
          this.itemStones.find(item => {
            if (i.id === item.id) {
              this.itemStones.splice(this.itemStones.indexOf(item), 1);
              this.emptySpaces.push(1);
              return true;
            }
          });
        });
      });
  }

}
