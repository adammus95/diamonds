import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {LoginService} from './login.service';
import {catchError, exhaustMap, retry, take, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {DataStorageService} from '../shared/data-storage.service';
import {AlertService} from '../shared/alert.service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private loginService: LoginService, private dataStorage: DataStorageService, private alertService: AlertService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return this.dataStorage.user.pipe(
      take(1),
      exhaustMap(user => {
        let finalRequest = req;
        if (user) {
          const token = user.token;
          finalRequest = req.clone({headers: req.headers.set('Authorization', token)});
        }
        return next.handle(finalRequest)
          .pipe(retry(1),
            catchError((error: HttpErrorResponse) => {
              error = error.error;
              const errorMessage = error.message;
              this.alertService.error(error.message);
              return throwError(errorMessage);
            }),
            tap(response => {
              return response;
            })
          );
      })
    );

  }

}
