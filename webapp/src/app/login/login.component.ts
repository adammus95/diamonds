import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from './login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  singupForm: FormGroup;
  isLoading = false;

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit(): void {
    this.singupForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    this.isLoading = true;
    this.loginService.signup(this.singupForm.value.username, this.singupForm.value.password).subscribe(response => {
      this.isLoading = false;
      this.router.navigate(['']);
    }, error => {
      this.isLoading = false;
    });
  }

}
