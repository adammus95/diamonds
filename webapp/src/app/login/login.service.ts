import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {User} from './user.model';
import {DataStorageService} from '../shared/data-storage.service';

@Injectable({providedIn: 'root'})
export class LoginService {

  // private url = 'https://gem-game-java.herokuapp.com/api/login';
  private url = 'http://localhost:8080/api/login';

  constructor(private http: HttpClient, private dataStorage: DataStorageService) {}

  signup(username: string, password: string) {
    return this.http.post(this.url, {username, password})
      .pipe(tap((resData: {user: User, token: string}) => {
        const user = new User(resData.user.username, resData.token, resData.user.userDetail);
        this.dataStorage.user.next(user);
      }));
  }

  autoLogin() {
    const userData: User = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return;
    }
    this.dataStorage.user.next(userData);
  }

  logout() {
    this.dataStorage.user.next(null);
  }
}
