import {UserDetails} from '../user-details/user-details.model';

export class User {
  constructor(public username: string, public token: string, public userDetail: UserDetails) {}

}
