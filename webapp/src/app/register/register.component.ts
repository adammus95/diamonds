import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {RequestsService} from '../shared/requests.service';
import {User} from '../login/user.model';
import {Router} from '@angular/router';
import {AlertService} from '../shared/alert.service';
import {LoginService} from '../login/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  singupForm: FormGroup;
  error = null;

  constructor(private requestsService: RequestsService, private router: Router, private alertService: AlertService, private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.singupForm = new FormGroup({
      username: new FormControl(null, [Validators.minLength(4), Validators.maxLength(16), Validators.required, Validators.pattern('^[a-z0-9]+$')]),
      password: new FormControl(null, [Validators.minLength(6), Validators.maxLength(30), Validators.required]),
      repeatPassword: new FormControl(null, [Validators.required, this.samePassword.bind(this)]),
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }

  samePassword(control: FormControl): { [s: string]: boolean } {
    if (this.singupForm && control.value !== this.singupForm.value.password) {
      return {passwordsAreDifferent: true};
    }
    return null;
  }

  onSubmit() {
    console.log(this.singupForm);
    this.requestsService.register(
      this.singupForm.value.email,
      this.singupForm.value.username,
      this.singupForm.value.password).subscribe((response: User) => {
        this.router.navigate(['/']);
        this.alertService.success('Register successful, you can now log in');
    });
  }

}
