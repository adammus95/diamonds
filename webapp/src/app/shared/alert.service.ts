import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Alert} from '../alert/alert.model';
import {AlertType} from '../alert/alert-type.enum';

@Injectable({providedIn: 'root'})
export class AlertService {

  private subject = new Subject<Alert>();

  constructor() {

  }

  clear() {
    this.subject.next(null);
  }

  success(message: string) {
    this.subject.next(new Alert(AlertType.SUCCESS, message, 5, null));
  }

  error(message: string) {
    this.subject.next(new Alert(AlertType.ERROR, message, 5, null));
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

}
