import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Auction} from '../auctions/auction.model';
import {AuctionService} from '../auctions/auction.service';
import {User} from '../login/user.model';
import {BehaviorSubject} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {Draw} from '../user-details/draw.model';
import {ItemStone} from '../items/item-stone.model';
import {UserInfo} from '../user/user-info.model';


@Injectable({providedIn: 'root'})
export class DataStorageService {

  user = new BehaviorSubject<User>(null);
  draws = new BehaviorSubject<Draw[]>(null);
  auctions = new BehaviorSubject<Auction[]>(null);
  itemStones = new BehaviorSubject<ItemStone[]>(null);
  dateTimeOffset = new Date().getTimezoneOffset() * 60 * 1000;
  conditionImage = new Image();

  constructor(private http: HttpClient, private auctionService: AuctionService) {
    this.conditionImage.src = 'assets/images/dirt.png';
    const itemStones = Array(1).fill(new ItemStone(true, 0, '-----', null, 0, 0, '000000000', false, false)).map(i => i);
    const auctions = Array(10).fill(new Auction(0, 0, false, '0', 0, new UserInfo('-----', 0), itemStones, null)).map(i => i);
    this.auctions.next(auctions);
  }

  getUserData() {
    return this.user.pipe(take(1), map(user => {
      if (user) {
        return user;
      } else {
        return null;
      }
    }));
  }

}
