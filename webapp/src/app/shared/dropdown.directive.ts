import {Directive, ElementRef, HostBinding, HostListener, OnInit} from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})

export class DropdownDirective implements OnInit {

  @HostBinding('class.show') open: boolean;

  constructor(private elRef: ElementRef) {}

  ngOnInit(): void {
    this.open = false;
  }
  // @HostListener('click') click() {
  //   this.open = !this.open;
  // }
  @HostListener('document:click', ['$event']) toggleOpen(event: Event) {
    this.open = this.elRef.nativeElement.contains(event.target) ? !this.open : false;
  }

}
