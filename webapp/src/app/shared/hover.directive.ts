import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
export class HoverClassDirective {

  constructor(public elementRef: ElementRef) { }
  @Input('appHover') hoverClass: any;

  @HostListener('mouseenter') onMouseEnter() {
    this.elementRef.nativeElement.classList.add(this.hoverClass);
    this.elementRef.nativeElement.style.cursor = 'pointer';
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.elementRef.nativeElement.classList.remove(this.hoverClass);
  }

}
