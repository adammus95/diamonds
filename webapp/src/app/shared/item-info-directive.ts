import {ContentChildren, Directive, ElementRef, HostListener, QueryList} from '@angular/core';
import {ItemStoneComponent} from '../items/item-stone/item-stone.component';

@Directive({
  selector: '[appItemInfo]'
})
export class ItemInfoDirective {

  @ContentChildren('info') private elementRefs: QueryList<ElementRef>;

  constructor(private itemComponent: ItemStoneComponent) {
  }

  @HostListener('mouseenter') onMouseEnter() {
    if (this.itemComponent.itemInfo && !this.itemComponent.displayItemInfo) {
      this.elementRefs.forEach((elRef: ElementRef) => {
        elRef.nativeElement.style.opacity = 1;
        elRef.nativeElement.style.transition = 'opacity 200ms ease';
      });
    }
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (this.itemComponent.itemInfo && !this.itemComponent.displayItemInfo) {
      this.elementRefs.forEach((elRef: ElementRef) => {
        elRef.nativeElement.style.opacity = 0;
        elRef.nativeElement.style.transition = 'opacity 200ms ease';
      });
    }
  }

}
