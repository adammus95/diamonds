import {MatDialog, MatDialogConfig} from '@angular/material';
import {ItemsPopupComponent} from '../items-popup/items-popup.component';
import {Injectable} from '@angular/core';
import {ItemStone} from '../items/item-stone.model';


@Injectable({providedIn: 'root'})
export class ItemsPopupService {

  constructor(private dialog: MatDialog) {
  }

  openPopup(data: {itemStones: ItemStone[], size: number}) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = data;
    this.dialog.open(ItemsPopupComponent, dialogConfig);
  }


}




