import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appMagnifier]'
})
export class MagnifierDirective {

  constructor(public elementRef: ElementRef) { }
  @Input('appMagnifier') hoverClass: any;

  @HostListener('mouseenter') onMouseEnter() {
    this.elementRef.nativeElement.classList.add(this.hoverClass);
    this.elementRef.nativeElement.style.cursor = 'pointer';
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.elementRef.nativeElement.classList.remove(this.hoverClass);
  }

}
