import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';


@Injectable({providedIn: 'root'})
export class RequestsService {

  // private url = 'https://gem-game-java.herokuapp.com/api';
  private url = 'http://localhost:8080/api';

  constructor(private http: HttpClient) {
  }

  getAuctions(auctionRequest: any) {
    const params: HttpParams = auctionRequest;
    for (const param in params) {
      if (params[param] == null) {
        delete params[param];
      }
    }

    return this.http.get(this.url + '/auction', {params});
  }

  signContract(itemStones: number[]) {
    return this.http.post(this.url + '/contract', {itemStones});
  }

  register(email: string, username: string, password: string) {
    return this.http.post(this.url + '/user/register', {email, username, password});
  }

  searchUser(partUsername: string) {
    return this.http.get(this.url + '/user/search', {params: {partUsername}});
  }

  addAuction(price: number, itemStones: number[], biddable: boolean) {
    return this.http.post(this.url + '/auction', {price, itemStones, biddable});
  }

  cut(id: number, cut: string, percent: number) {
    return this.http.post(this.url + '/cut', {id, cut, percent});
  }

  getLastDraws() {
    return this.http.get(this.url + '/draw/last');
  }

  drawStone() {
    return this.http.post(this.url + '/item/draw', null);
  }

  drawGhostStones(size: number) {
    return this.http.get(this.url + '/item/draw/ghost', {params: {size: size.toString()}});
  }

  buy(id: number) {
    return this.http.post(this.url + '/auction/' + id, null);
  }

  bid(id: number, price: number) {
    return this.http.post(this.url + '/bid', {id, price});
  }

  getUserDetails() {
    return this.http.get(this.url + '/user/details');
  }

  getLastUserDraw() {
    return this.http.get(this.url + '/draw');
  }

  getUserItemStones(username: string, size: number, page: number, sort: string, direction: string) {
    return this.http.get(this.url + '/item/' + username,
      {params: {page: page.toString(), size: size.toString(), sort, direction}});
  }

}
