import {Injectable} from '@angular/core';

export class Time {
  constructor(public formatedTime: string, public days: number, public hours: number, public minutes: number, public seconds: number) {
  }
}

@Injectable({providedIn: 'root'})
export class TimeService {


  constructor() {
  }


  timestampToLocalTime(expDate: string) {

    const h = (expDate[3] != null) ? Number(expDate[3]) : 0;
    const m = (expDate[4] != null) ? Number(expDate[4]) : 0;
    const s = (expDate[5] != null) ? Number(expDate[5]) : 0;
    const date = new Date(Number(expDate[0]), Number(expDate[1]) - 1, Number(expDate[2]),
      h, m, s);
    return date;

  }

  getTime(seconds: number) {
    const time = new Time('', 0, 0,  0, 0);
    if (seconds < 60) {
      time.formatedTime = Math.floor(seconds) + ' s';
      time.seconds = Math.floor(seconds);
    } else if (seconds >= 60 && seconds < 3600) {
      time.formatedTime = Math.floor(seconds / 60) + ' min ' + Math.floor(seconds % 60) + ' s';
      time.minutes = Math.floor(seconds / 60)
      time.seconds = Math.floor(seconds % 60);
    } else if (seconds >= 3600 && seconds < 86400) {
      time.formatedTime = Math.floor(seconds / 3600) + ' h ' + Math.floor((seconds % 3600) / 60) + ' min';
      time.hours = Math.floor(seconds / 3600);
      time.minutes = Math.floor((seconds % 3600) / 60);
    } else {
      time.formatedTime = Math.floor(seconds / 86400) + ' d ' + Math.floor((seconds % 86400) / 3600) + ' h';
      time.days = Math.floor(seconds / 86400);
      time.hours = Math.floor((seconds % 86400) / 3600);
    }
    return time;
  }

}

