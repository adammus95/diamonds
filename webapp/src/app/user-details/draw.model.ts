import {ItemStone} from '../items/item-stone.model';

export class Draw {
  constructor(public drawTime: string, public itemStone: ItemStone) {}
}
