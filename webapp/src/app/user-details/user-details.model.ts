import {Draw} from './draw.model';

export class UserDetails {
  constructor(public money: number, public draws: Draw) {}
}
