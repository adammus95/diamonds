export class UserInfo {
  constructor(public username: string, private id: number) {}

  get _id() {
    return this.id;
  }
}
