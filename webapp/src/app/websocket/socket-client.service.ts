import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {SocketClientState} from './socket-client-state.enum';
import {CompatClient, Stomp, StompSubscription} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {filter, first, switchMap} from 'rxjs/operators';
import {DataStorageService} from '../shared/data-storage.service';

@Injectable({
  providedIn: 'root'
})
export class SocketClientService implements OnDestroy {

  private client: CompatClient;
  private state: BehaviorSubject<SocketClientState>;

  private url = 'http://localhost:8080/api';
  // private url = 'https://gem-game-java.herokuapp.com/api';

  constructor(private dataStorage: DataStorageService) {
    this.client = Stomp.over(new SockJS(this.url + '/websocket'));
    this.state = new BehaviorSubject<SocketClientState>(SocketClientState.ATTEMPTING);
    this.client.connect({}, () => {
      this.state.next(SocketClientState.CONNECTED);
    });
  }

  private connect(): Observable<CompatClient> {
    return new Observable<CompatClient>(observer => {
      this.state.pipe(filter(state => state === SocketClientState.CONNECTED)).subscribe(() => {
        observer.next(this.client);
      });
    });
  }

  onMessage(topic: string): Observable<any> {
    return this.connect().pipe(first(), switchMap(client => {
      return new Observable<any>(observer => {
        const subscription: StompSubscription = client.subscribe(topic, message => {
          observer.next(message.body);
        });
        return () => client.unsubscribe(subscription.id);
      });
    }));
  }

  ngOnDestroy() {
    this.connect().pipe(first()).subscribe(inst => inst.disconnect(null));
  }

  send(topic: string): void {
    this.connect()
      .pipe(first())
      .subscribe(inst => inst.send(topic, {}));
  }
}
